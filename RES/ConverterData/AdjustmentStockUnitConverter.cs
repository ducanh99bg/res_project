﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RES.ConverterData
{
    class AdjustmentStockUnitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Inventory item = value as Inventory;
            string stock = mdlMain.dicStockUnit[item.stockunit];
            return stock;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
