﻿using System;
using System.Globalization;
using System.Windows.Data;
using RES.Models;

namespace RES.ConverterData
{
    class GenderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Employee item = value as Employee;
            if(item == null)
            {
                return null;
            }
            string genderName = (item.gender == 0) ? "Nữ" : "Nam";
            return genderName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
