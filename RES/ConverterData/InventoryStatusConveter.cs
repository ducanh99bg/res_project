﻿using RES.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace RES.ConverterData
{
    class InventoryStatusConveter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Inventory item = value as Inventory;
            if(item == null)
            {
                return null;
            }
            return (item.status == 2) ? "Đang hoạt động" : "Khóa";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
