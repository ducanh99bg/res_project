﻿using System;
using System.Globalization;
using System.Windows.Data;
using RES.Models;

namespace RES.ConverterData
{
    class DepartmentStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Department item = value as Department;
            if(item == null)
            {
                return null;
            }
            string status = (item.status == 1) ? "Khóa" : "Đang hoạt động";
            return status;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
