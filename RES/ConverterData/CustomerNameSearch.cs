﻿using RES.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace RES.ConverterData
{
    class CustomerNameSearch : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Customer item = value as Customer;
            string result = "";
            result = item.firstname + item.lastname + "(" + item.phone + ")";
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
