﻿using RES.Models;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Linq;

namespace RES.ConverterData
{
    class DepartmentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Category item = value as Category;
            if(item == null)
            {
                return null;
            }
            string deName = "";
            using(var context = new RESContext())
            {
                deName = context.Departments.FirstOrDefault(x => x.de_uid == item.de_uid).name;
            }
            return deName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
