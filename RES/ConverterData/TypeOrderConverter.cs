﻿using RES.Common;
using RES.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace RES.ConverterData
{
    class TypeOrderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Order order = value as Order;
            string type = mdlMain.dicTypeSale[order.type];
            return type;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
