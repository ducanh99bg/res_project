﻿using RES.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RES.ConverterData
{
    class MenuItemPriceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Product item = value as Product;
            if(item == null)
            {
                return null;
            }
            double price = 0;
            double.TryParse(item.price + "", out price);
            using(var context = new RESContext())
            {
                //List<Promotion> lstPro = context.Promotions.Where(x => x.)ToList();
            };
            return price;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
