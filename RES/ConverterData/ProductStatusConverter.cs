﻿using RES.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace RES.ConverterData
{
    class ProductStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Product item = value as Product;
            if(item == null)
            {
                return null;
            }
            string status = (item.status == 1) ? "Khóa" : "Đang hoạt động";
            return status;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
