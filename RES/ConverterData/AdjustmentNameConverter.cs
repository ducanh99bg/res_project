﻿using RES.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RES.ConverterData
{
    class AdjustmentNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Inventory item = value as Inventory;
            string name = "";
            using(var context =  new RESContext())
            {
                name = context.Vendors.FirstOrDefault(x => x.ven_uid == item.ven_uid).name;
            }
            return name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
