﻿using RES.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RES.ConverterData
{
    class ProductCategoryNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Product item = value as Product;
            if(item == null)
            {
                return null;
            }
            string caname = "";
            using(var context = new RESContext())
            {
                Category ca = context.Categories.FirstOrDefault(x => x.ca_uid == item.ca_uid);
                caname = ca.name;
            }
            return caname;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
