﻿using System;
using System.Globalization;
using System.Windows.Data;
using RES.Models;

namespace RES.ConverterData
{
    class FullnameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Employee item = value as Employee;
            if (item == null)
            {
                return null;
            }
            string name = item.firstname + "" + item.lastname;
            return name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
