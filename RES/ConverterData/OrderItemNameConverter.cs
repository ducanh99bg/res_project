﻿using RES.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RES.ConverterData
{
    class OrderItemNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            OrderDetail item = value as OrderDetail;
            string name = "";
            using(var context = new RESContext())
            {
                name = context.Products.FirstOrDefault(x => x.product_uid == item.po_uid).name;
            }
            return name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
