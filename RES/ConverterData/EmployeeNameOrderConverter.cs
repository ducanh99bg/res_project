﻿using RES.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RES.ConverterData
{
    class EmployeeNameOrderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Order order = value as Order;
            string name = "";
            using(var context = new RESContext())
            {
                name = context.Employees.FirstOrDefault(x => x.emp_uid == order.emp_uid).firstname;
                name += context.Employees.FirstOrDefault(x => x.emp_uid == order.emp_uid).lastname;
            }
            return name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
