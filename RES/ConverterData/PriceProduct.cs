﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RES.ConverterData
{
    class PriceProduct : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Product item = value as Product;
            double discount = 0;
            List<PromotionDetail> promotionProduct = new List<PromotionDetail>();
            List<Promotion> promotions = mdlMain.getPromotions();
            using (var context = new RESContext())
            {
                foreach (Promotion i in promotions)
                {
                    promotionProduct = context.PromotionDetails.Where(x => x.pro_uid == i.pro_uid).ToList();
                    promotionProduct = promotionProduct.Where(x => x.product_id == item.product_uid).ToList();
                    if(promotionProduct.Count > 0)
                    {
                        break;
                    }
                }
            }
            foreach (PromotionDetail i in promotionProduct)
            {
                discount += i.discount_product;
            }
            return (item.price * (1 - discount * 0.01)).ToString("###,###,###,##0") + " VND";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
