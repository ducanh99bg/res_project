﻿using RES.Common;
using RES.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace RES.ConverterData
{
    class InventoryStockUnitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Inventory item = value as Inventory;
            if (item == null)
            {
                return null;
            }
            string stockunit = mdlMain.dicStockUnit[item.stockunit].ToString();
            return stockunit;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
