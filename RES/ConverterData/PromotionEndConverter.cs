﻿using RES.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace RES.ConverterData
{
    class PromotionEndConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Promotion item = value as Promotion;
            if (item == null)
            {
                return null;
            }
            string date = item.end.ToString("dd-MM-yyyy");
            return date;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
