﻿using RES.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RES.ConverterData
{
    class CustomerNameOrderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Order order = value as Order;
            string name = "";
            using (var context = new RESContext())
            {
                Customer cus = context.Customers.FirstOrDefault(x => x.cus_uid == order.cus_uid);
                if(cus != null)
                {
                    name = cus.firstname + " " + cus.lastname;
                }
            }
            return name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
