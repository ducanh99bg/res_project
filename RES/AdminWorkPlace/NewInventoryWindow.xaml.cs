﻿using Microsoft.Win32;
using RES.Common;
using RES.Models;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for NewInventoryWindow.xaml
    /// </summary>
    public partial class NewInventoryWindow : Window
    {
        internal string imgPath = "";
        public NewInventoryWindow()
        {
            InitializeComponent();
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbxType.ItemsSource = mdlMain.dicInventoryType.Where(x => x.Key != 0);
            cbxStockUnit.ItemsSource = mdlMain.dicStockUnit.Where(x => x.Key != 0);
            cbxVendor.ItemsSource = mdlMain.getVendor().Where(x => x.Key != 0);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "c:\\Users\\chudu\\source\\repos\\RES\\RES\\img\\inventory";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            if (dialog.ShowDialog() == true)
            {
                string selectedFileName = dialog.FileName;
                //Copy to new folder
                string filename = System.IO.Path.GetFileName(selectedFileName);
                var environment = Environment.CurrentDirectory;
                string dicPath = Directory.GetParent(environment).Parent.FullName + "\\img\\inventory\\" + filename;
                if (!File.Exists(dicPath))
                {
                    File.Copy(selectedFileName, dicPath);
                }
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Directory.GetParent(environment).Parent.FullName + "\\img\\inventory\\" + filename);
                bitmap.EndInit();
                imgViewer.Source = bitmap;

                imgPath = "\\img\\inventory\\" + filename;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    string name = txtName.Text + "";
                    int type = cbxType.SelectedIndex + 1;
                    string img = imgPath;
                    double cost = 0;
                    double.TryParse(txtCost.Text + "", out cost);
                    double quatity = 0;
                    double.TryParse(txtQuatity.Text + "", out quatity);
                    int stock = cbxStockUnit.SelectedIndex + 1;
                    int status = (isActive.IsChecked == true) ? 2 : 1;

                    string vendor = mdlMain.getVendor()[cbxVendor.SelectedIndex + 1];
                    Guid venId = context.Vendors.FirstOrDefault(x => x.name == vendor).ven_uid;
                    Vendor ven = context.Vendors.FirstOrDefault(x => x.ven_uid == venId);

                    Inventory inven = new Inventory()
                    {
                        inv_uid = Guid.NewGuid(),
                        name = name,
                        type = type,
                        img = img,
                        quatity = quatity,
                        cost = cost,
                        stockunit = stock,
                        status = status,
                        ven_uid = venId,
                        Vendor = ven
                    };
                    context.Inventorys.Add(inven);
                    context.SaveChanges();
                }
            }
            catch(Exception ex)
            {

            }
            this.Close();
        }
    }
}
