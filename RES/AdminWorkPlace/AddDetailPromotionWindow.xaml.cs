﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for AddDetailPromotionWindow.xaml
    /// </summary>
    public partial class AddDetailPromotionWindow : Window
    {
        internal Guid ProId;
        internal List<PromotionDetail> promotiondetails;
        internal PromotionDetail promotionDiscountTotal;
        internal PromotionDetail promotionDisCountCate;


        public AddDetailPromotionWindow()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    if(rdBill.IsChecked == true)
            //    {
            //        double start = 0;
            //        double.TryParse(txtFrom.Text + "", out start);
            //        double discount = 0;
            //        double.TryParse(txtDiscountTotalBill.Text + "", out discount);
            //        //Sale for total bill
            //        if(promotionDiscountTotal == null)
            //        {
            //            PromotionDetail detail = new PromotionDetail()
            //            {
            //                prodetail_uid = Guid.NewGuid(),
            //                pro_uid = ProId,
            //                type = 1,
            //                product_id = new Guid(),
            //                discount_product = 0,
            //                start_value = start,
            //                discount_totalbill = discount
            //            };
            //            using (var context = new RESContext())
            //            {
            //                context.PromotionDetails.Add(detail);
            //                context.SaveChanges();
            //            }
            //        }
            //        else
            //        {
            //            using(var context = new RESContext())
            //            {
            //                PromotionDetail promotionDetail = context.PromotionDetails.FirstOrDefault(x => x.prodetail_uid == promotionDiscountTotal.prodetail_uid);
            //                promotionDetail.start_value = start;
            //                promotionDetail.discount_totalbill = discount;
            //                context.SaveChanges();
            //            }
                        
            //        }
                    
            //    }
            //    if(rdCategory.IsChecked == true)
            //    {
            //        //Sale for all product in category
            //        using (var context = new RESContext())
            //        {
            //            int s = cbxCategory.SelectedIndex;
            //            string name = mdlMain.getCategory()[s + 1];
            //            Guid cateCode = context.Categories.FirstOrDefault(x => x.name == name).ca_uid;
            //            List<Product> products = context.Products.Where(x => x.ca_uid == cateCode).ToList();

            //            double discount = 0;
            //            double.TryParse(txtDiscountCategory.Text + "", out discount);

            //            foreach (Product item in products)
            //            {
            //                PromotionDetail detail = new PromotionDetail()
            //                {
            //                    prodetail_uid = Guid.NewGuid(),
            //                    pro_uid = ProId,
            //                    type = 2,
            //                    product_id = item.product_uid,
            //                    discount_product = discount,
            //                    discount_totalbill = 0,
            //                    start_value = 0,
            //                };
            //                context.PromotionDetails.Add(detail);
            //            }
            //            context.SaveChanges();
            //        };
            //        this.Close();
            //    }
            //}
            //catch(Exception ex)
            //{

            //}
        }

        public void SetData()
        {
            //if(promotionDiscountTotal != null)
            //{
            //    rdBill.IsChecked = true;
            //    txtFrom.Text = promotionDiscountTotal.start_value + "";
            //    txtDiscountTotalBill.Text = promotionDiscountTotal.discount_totalbill + "";
            //}
            //if(promotionDisCountCate != null)
            //{
            //    rdCategory.IsChecked = true;
            //    txtDiscountCategory.Text = promotionDisCountCate.discount_product + "";
            //}
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //using(var context = new RESContext()) {
            //    promotiondetails = context.PromotionDetails.Where(x => x.pro_uid == ProId).ToList();
            //    promotionDiscountTotal = promotiondetails.FirstOrDefault(x => x.type == 1);
            //    promotionDisCountCate = promotiondetails.FirstOrDefault(x => x.type == 2);
            //}
            //cbxCategory.ItemsSource = mdlMain.getCategory().Where(x => x.Key != 0);
        }
    }
}
