﻿using RES.Models;
using System;
using System.Windows;
using System.Linq;
using System.Windows.Media.Imaging;
using System.IO;
using RES.Common;
using System.Collections.Generic;
using Microsoft.Win32;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DetailProductWindow.xaml
    /// </summary>
    public partial class DetailProductWindow : Window
    {
        internal Guid productId;
        internal Product product;
        internal string strImg;
        public DetailProductWindow()
        {
            InitializeComponent();
            product = new Product();
        }

        public void SetData()
        {
            using(var context = new RESContext())
            {
                product = context.Products.FirstOrDefault(x => x.product_uid == productId);
            }
            var environment = Environment.CurrentDirectory;
            string dicPath = Directory.GetParent(environment).Parent.FullName + product.img.ToString();
            if (File.Exists(dicPath))
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Directory.GetParent(environment).Parent.FullName + product.img);
                bitmap.EndInit();
                imgViewer.Source = bitmap;
                strImg = product.img;
            }
            txtName.Text = product.name + "";
            txtDescription.Text = product.description + "";
            txtPrice.Text = product.price + "";
            Guid cacode = product.ca_uid;
            string caname = "";
            using(var context = new RESContext())
            {
                caname = context.Categories.FirstOrDefault(x => x.ca_uid == cacode).name;
                cbxCategory.SelectedIndex = mdlMain.dicCate.FirstOrDefault(x => x.Value == caname).Key;
            }
            cbxActive.IsChecked = (product.status == 2) ? true : false;
            cbxForSale.IsChecked = (product.forsale == 1) ? true : false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbxCategory.ItemsSource = mdlMain.getCategory().Where(x => x.Key != 0);
            SetData();
        }

        private void btnCacel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void imgViewer_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "c:\\";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            if (dialog.ShowDialog() == true)
            {
                string selectedFileName = dialog.FileName;
                //Copy to new folder
                string filename = Path.GetFileName(selectedFileName);
                var environment = Environment.CurrentDirectory;
                string dicPath = Directory.GetParent(environment).Parent.FullName + "\\img\\product\\" + filename;
                if (!File.Exists(dicPath))
                {
                    File.Copy(selectedFileName, dicPath);
                }
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Directory.GetParent(environment).Parent.FullName + "\\img\\product\\" + filename);
                bitmap.EndInit();
                imgViewer.Source = bitmap;
                strImg = "\\img\\product\\" + filename;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            using(var context = new RESContext())
            {
                string name = txtName.Text + "";
                string des = txtDescription.Text + "";
                double price = 0;
                double.TryParse(txtPrice.Text + "", out price);
                short status = (cbxActive.IsChecked == true) ? (short)2 : (short)1;
                short forsale = (cbxForSale.IsChecked == true) ? (short)1 : (short)2;
                DateTime modifieddate = DateTime.Now;

                try
                {
                    var product = context.Products.FirstOrDefault(x => x.product_uid == productId);
                    product.name = name;
                    product.description = des;
                    product.price = price;
                    product.modified = modifieddate;
                    product.forsale = forsale;
                    product.status = status;
                    product.forsale = forsale;
                    context.SaveChanges();
                }
                catch(Exception ex)
                {
                    this.Close();
                }
                this.Close();
            }
        }
    }
}
