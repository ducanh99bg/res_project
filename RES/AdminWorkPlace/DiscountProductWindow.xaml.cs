﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DiscountProductWindow.xaml
    /// </summary>
    public partial class DiscountProductWindow : Window
    {
        internal List<PromotionDetail> promotions;
        internal Guid promotion_uid;

        public class ProductDiscount
        {
            public Guid product_uid { get; set; }
            public Guid cate_uid { get; set; }
            public string productName { get; set; }
            public double discount { get; set; }
            public double price { get; set; }
        }
        internal List<ProductDiscount> productDiscounts;

        public DiscountProductWindow()
        {
            InitializeComponent();
        }

        public void SetItemSource()
        {
            using (var context = new RESContext())
            {
                productDiscounts = new List<ProductDiscount>();
                //SetListViewItemSource
                foreach (Product item in context.Products.ToList())
                {
                    PromotionDetail detail = context.PromotionDetails.FirstOrDefault(x => (x.pro_uid == promotion_uid) && (x.product_id == item.product_uid));
                    ProductDiscount productDiscount = new ProductDiscount()
                    {
                        product_uid = item.product_uid,
                        cate_uid = item.ca_uid,
                        productName = item.name,
                        discount = (detail == null) ? 0 : detail.discount_product,
                        price = (detail == null) ? item.price : (item.price - (1 - detail.discount_product / 100))
                    };
                    productDiscounts.Add(productDiscount);
                };
                if (cbxCategory.SelectedIndex != 0)
                {
                    string cateName = mdlMain.getCategory()[cbxCategory.SelectedIndex].ToString();
                    Category cate = context.Categories.FirstOrDefault(x => x.name.CompareTo(cateName) == 0);
                    productDiscounts = productDiscounts.Where(x => x.cate_uid == cate.ca_uid).ToList();
                }
            }
            lvProduct.ItemsSource = productDiscounts.ToList();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                cbxCategory.ItemsSource = mdlMain.getCategory();
                SetItemSource();
            }
            catch (Exception ex)
            {

            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    foreach (ProductDiscount item in lvProduct.Items)
                    {
                        PromotionDetail detail = context.PromotionDetails.FirstOrDefault(x => x.pro_uid == promotion_uid && x.product_id == item.product_uid);
                        if(detail == null)
                        {
                            detail = new PromotionDetail
                            {
                                prodetail_uid = Guid.NewGuid(),
                                pro_uid = promotion_uid,
                                type = 1,
                                product_id = item.product_uid,
                                discount_product = item.discount
                            };
                            context.PromotionDetails.Add(detail);
                        }
                        else
                        {
                            detail.discount_product = item.discount;
                        }
                        context.SaveChanges();
                    }
                }
                this.Close();
            }
            catch (Exception ex)
            {

            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lvProduct_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            
        }

        private void cbxCategory_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void txtDiscount_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {

            }
        }
    }
}
