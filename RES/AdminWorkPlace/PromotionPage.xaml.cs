﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for PromotionPage.xaml
    /// </summary>
    public partial class PromotionPage : Page
    {
        internal List<Promotion> lstPromotion;
        public PromotionPage()
        {
            InitializeComponent();
            lstPromotion = new List<Promotion>();
        }

        public void SetItemSource()
        {
            string search = "";
            int status = -1;
            try
            {
                search = txtSearch.Text + "";
                status = cbxStatus.SelectedIndex;

                using(var context = new RESContext())
                {
                    lstPromotion = context.Promotions.ToList();
                    if (!string.IsNullOrEmpty(search))
                    {
                        lstPromotion = lstPromotion.Where(x => x.title.Contains(search)).ToList();
                    }
                    if(status != 0)
                    {
                        lstPromotion = lstPromotion.Where(x => x.status == status).ToList();
                    }
                }
                lvPromotion.ItemsSource = lstPromotion;
            }
            catch(Exception ex)
            {

            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            cbxStatus.ItemsSource = mdlMain.dicStatus;
            SetItemSource();
        }

        private void cbxStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void btnPromotionNew_Click(object sender, RoutedEventArgs e)
        {
            NewPromotionWindow newPromotion = new NewPromotionWindow();
            newPromotion.ShowDialog();
            SetItemSource();
        }

        private void lvPromotion_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ListView items = sender as ListView;
                Promotion pro = (Promotion)items.SelectedItem;
                if (pro != null)
                {
                    DetailPromotionWindow de = new DetailPromotionWindow();
                    de.id = pro.pro_uid;
                    de.ShowDialog();
                }
            }
            catch (Exception ex)
            {

            }
            SetItemSource();
        }
    }
}
