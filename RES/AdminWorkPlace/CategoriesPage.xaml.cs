﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using RES.Models;
using RES.Common;
using System.Collections.Generic;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for CategoriesPage.xaml
    /// </summary>
    public partial class CategoriesPage : Page
    {
        internal List<Category> lst;
        public CategoriesPage()
        {
            InitializeComponent();
            
        }

        private void SetItemSource(string search, int department, int status)
        {
            using(var context = new RESContext())
            {
                lst = context.Categories.ToList();
                if(status != 0)
                {
                    lst = lst.Where(x => x.status == status).ToList();
                }
                if (department != 0)
                {
                    string deName = mdlMain.dicDe[department];
                    Guid decode = context.Departments.FirstOrDefault(x => x.name == deName).de_uid;
                    lst = lst.Where(x => x.de_uid == decode).ToList();
                }
                if (!string.IsNullOrEmpty(search))
                {
                    lst = lst.Where(x => x.name.Contains(search)).ToList();
                }
                lvCategories.ItemsSource = lst;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            cbxDepartment.ItemsSource = mdlMain.getDepartment();
            cbxCategoryStatus.ItemsSource = mdlMain.dicStatus;
            SetItemSource(txtSearch.Text + "", cbxDepartment.SelectedIndex, cbxCategoryStatus.SelectedIndex);

        }

        private void btnCategoryNew_Click(object sender, RoutedEventArgs e)
        {
            NewCategoriesWindow newCa = new NewCategoriesWindow();
            newCa.ShowDialog();
            SetItemSource(txtSearch.Text+"", cbxDepartment.SelectedIndex, cbxCategoryStatus.SelectedIndex);
        }

        private void cbxCategoryAll_Checked(object sender, RoutedEventArgs e)
        {
            lvCategories.SelectAll();
        }

        private void cbxCategoryAll_Unchecked(object sender, RoutedEventArgs e)
        {
            lvCategories.UnselectAll();
        }

        private void btnCategoryDelete_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new RESContext())
            {
                var selectedItem = lvCategories.SelectedItems;
                for (int i = 0; i < selectedItem.Count; i++)
                {
                    Guid guid = Guid.Parse(selectedItem[i].GetType().GetProperties().First(x => x.Name == "ca_uid").GetValue(selectedItem[i], null) + "");
                    var currentCategory = context.Categories.FirstOrDefault(x => x.ca_uid == guid);
                    currentCategory.status = 1;
                    foreach(Product po in context.Products.Where(x => x.ca_uid == currentCategory.ca_uid).ToList())
                    {
                        var product = context.Products.FirstOrDefault(x => x.product_uid == po.product_uid);
                        product.status = 1;
                        context.SaveChanges();
                    }
                    //context.SaveChanges();
                }
            }
            SetItemSource(txtSearch.Text + "", cbxDepartment.SelectedIndex, cbxCategoryStatus.SelectedIndex);
        }

        private void cbxDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource(txtSearch.Text + "", cbxDepartment.SelectedIndex, cbxCategoryStatus.SelectedIndex);
        }

        private void cbxCategoryStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource(txtSearch.Text + "", cbxDepartment.SelectedIndex, cbxCategoryStatus.SelectedIndex);
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource(txtSearch.Text + "", cbxDepartment.SelectedIndex, cbxCategoryStatus.SelectedIndex);
        }

        private void lvCategories_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                ListView items = sender as ListView;
                Category cate = (Category)items.SelectedItem;
                if (cate != null)
                {
                    DetailCategoryWindow de = new DetailCategoryWindow();
                    de.cate_uid = cate.ca_uid;
                    de.ShowDialog();
                }
            }
            catch (Exception ex)
            {

            }
            SetItemSource(txtSearch.Text + "", cbxDepartment.SelectedIndex, cbxCategoryStatus.SelectedIndex);
        }
    }
}
