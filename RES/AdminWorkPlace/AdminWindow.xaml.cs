﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using RES.Models;
using RES.Common;
using System.Windows.Controls;
using Microsoft.Win32;
using System.IO;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        /*Employee*/
        internal List<Employee> tmp;

        public AdminWindow()
        {
            InitializeComponent();

            tmp = new List<Employee>();
        }
        //Set ItemSource for lvEmployee
        private void SetEmployeeItemSource(string strSearch, int position, int status)
        {
            //Fill data to list view
            try
            {
                using (var context = new RESContext())
                {
                    tmp = context.Employees.ToList();
                    if (status != 0)
                    {
                        tmp = tmp.Where(x => x.status == status).ToList();
                    }
                    if (position != 0)
                    {
                        tmp = tmp.Where(x => context.Accounts.FirstOrDefault(y => y.ac_uid == x.ac_uid).role == position).ToList();
                    }
                    if (!string.IsNullOrEmpty(strSearch))
                    {
                        tmp = tmp.Where(x => (x.firstname.Contains(strSearch) | x.lastname.Contains(strSearch))).ToList();
                    }
                    lvEmployee.ItemsSource = tmp;
                }
            }
            catch(Exception ex)
            {

            }
        }
        //Exit
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        //Onloaded Admin window
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Set itemsource for combobox position
            cbxPosition.ItemsSource = mdlMain.dicPosition;
            //Set itemsource for cbxStatus
            cbxStatus.ItemsSource = mdlMain.dicStatus;
            /*Start Employee*/
            SetEmployeeItemSource(txtSearch.Text + "".Trim(), cbxPosition.SelectedIndex, cbxStatus.SelectedIndex);
            /*End Employee*/
            /*Start Inventory*/
            InventoryFrame.Navigate(new DepartmentPage());
            /*End Inventory*/

            /*Promotion*/
            PromotionFrame.Navigate(new PromotionPage());
            /*Customer*/
            CustomerFrame.Navigate(new CustomerPage());
            /*Vendor*/
            VendorFrame.Navigate(new VendorPage());

            /*Report*/
            ReportFrame.Navigate(new ReportSaleProductPage());
        }
        private void TabablzControl_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
        private void txtSearch_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            SetEmployeeItemSource(txtSearch.Text + "".Trim(), cbxPosition.SelectedIndex, cbxStatus.SelectedIndex);
        }
        //Employee
        private void btnEmployeeNew_Click(object sender, RoutedEventArgs e)
        {
            NewEmpWindow empAdd = new NewEmpWindow();
            empAdd.ShowDialog();
            SetEmployeeItemSource(txtSearch.Text + "", cbxPosition.SelectedIndex, cbxStatus.SelectedIndex);
        }
        //Employee Position Change
        private void cbxPosition_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            SetEmployeeItemSource(txtSearch.Text + "".Trim(), cbxPosition.SelectedIndex, cbxStatus.SelectedIndex);
        }
        //Status Employee Change
        private void cbxStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetEmployeeItemSource(txtSearch.Text + "".Trim(), cbxPosition.SelectedIndex, cbxStatus.SelectedIndex);
        }
        //Double click in lv Employee
        private void lvEmployee_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                ListView items = sender as ListView;
                Employee emp = (Employee)items.SelectedItem;
                if(emp != null)
                {
                    DetailEmpWindow detail = new DetailEmpWindow();
                    detail.empId = emp.emp_uid;
                    detail.ShowDialog();
                }
                SetEmployeeItemSource(txtSearch.Text + "", cbxPosition.SelectedIndex, cbxStatus.SelectedIndex);
            }
            catch(Exception ex)
            {

            }
        }

        private void cbxEmpCheckAll_Checked(object sender, RoutedEventArgs e)
        {
            lvEmployee.SelectAll();
        }

        private void cbxEmpCheckAll_Unchecked(object sender, RoutedEventArgs e)
        {
            lvEmployee.UnselectAll();
        }

        private void btnEmployeeDelete_Click(object sender, RoutedEventArgs e)
        {
            using(var context  = new RESContext())
            {
                var selectedItem = lvEmployee.SelectedItems;
                for (int i = 0; i < selectedItem.Count; i++)
                {
                    Guid guid = Guid.Parse(selectedItem[i].GetType().GetProperties().First(x => x.Name == "emp_uid").GetValue(selectedItem[i], null) + "");
                    var currentEmp = context.Employees.FirstOrDefault(x => x.emp_uid == guid);
                    currentEmp.status = 1;
                    context.SaveChanges();
                }
            };
        }

        private void btnEmployeeImport_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ex = new OpenFileDialog();
            ex.InitialDirectory = "c:\\";
            ex.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            ex.RestoreDirectory = true;
            if (ex.ShowDialog() == true)
            {
                string selectedFileName = ex.FileName;
                //Copy to new folder
                string filename = Path.GetFileName(selectedFileName);

                List<Employee> lst = ReadEmployeeCsvFile(selectedFileName);

                using (var context = new RESContext())
                {
                    foreach(Employee item in lst)
                    {
                        Account a = item.Account;
                        context.Accounts.Add(a);
                        Employee emp = item;
                        context.Employees.Add(emp);
                    }
                    context.SaveChanges();

                }
            }
            SetEmployeeItemSource(txtSearch.Text + "", cbxPosition.SelectedIndex, cbxStatus.SelectedIndex);

        }

        private List<Employee> ReadEmployeeCsvFile(string path)
        {
            try
            {
                string[] csvLines = File.ReadAllLines(path);

                var employees = new List<Employee>();

                for (int i = 1; i < csvLines.Length; i++)
                {
                    string rowData = csvLines[i];
                    string[] data = rowData.Split(',');
                    Account acc = new Account()
                    {
                        ac_uid = Guid.NewGuid(),
                        username = data[1],
                        password = "123",
                        role = 0,
                        created = DateTime.Now,
                        modified = DateTime.Now,
                        status = 2
                    };
                    Employee emp = new Employee()
                    {
                        emp_uid = Guid.NewGuid(),
                        ac_uid = acc.ac_uid,
                        firstname = data[1],
                        lastname = data[2],
                        gender = (data[3].ToString().CompareTo("Male") == 0) ? (short)1 : (short)0,
                        birthdate = DateTime.Now,
                        address = data[5],
                        phone = data[6],
                        startdate = DateTime.Now,
                        created = DateTime.Now,
                        modified = DateTime.Now,
                        img = "",
                        status = Convert.ToInt16(data[8]),
                        Account = acc
                    };
                    employees.Add(emp);
                }

                return employees;
            }
            catch(Exception ex)
            {
                return null;
            }
            
        }

        //Load Page
        private void btnDepartment_Click(object sender, RoutedEventArgs e)
        {
            InventoryFrame.Navigate(new DepartmentPage());
        }

        private void btnCategory_Click(object sender, RoutedEventArgs e)
        {
            InventoryFrame.Navigate(new CategoriesPage());
        }

        private void btnProduct_Click(object sender, RoutedEventArgs e)
        {
            InventoryFrame.Navigate(new ProductPage());
        }

        private void btnInventory_Click(object sender, RoutedEventArgs e)
        {
            InventoryFrame.Navigate(new InventoryPage());
        }

        private void btnSaleReport_Click(object sender, RoutedEventArgs e)
        {
            ReportFrame.Navigate(new ReportSaleProductPage());
        }

        private void btnProductReport_Click(object sender, RoutedEventArgs e)
        {
            ReportFrame.Navigate(new ReportProductPage());
        }

        private void btnReportRevenue_Click(object sender, RoutedEventArgs e)
        {
            ReportFrame.Navigate(new ReportRevenuePage());
        }
        //EndLoad Page
    }
}
