﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Input;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for VendorPage.xaml
    /// </summary>
    public partial class VendorPage : Page
    {
        internal List<Vendor> lstven;
        public VendorPage()
        {
            InitializeComponent();
            lstven = new List<Vendor>();
        }

        public void SetItemSource()
        {
            string search = txtSearch.Text + "";
            int status = cbxStatus.SelectedIndex;
            using(var context = new RESContext())
            {
                lstven = context.Vendors.ToList();
                if (!string.IsNullOrEmpty(search))
                {
                    lstven = lstven.Where(x => x.name.Contains(search)).ToList();
                }
                if(status != 0)
                {
                    lstven = lstven.Where(x => x.status == status).ToList();
                }
            }
            lvVendor.ItemsSource = lstven;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            cbxStatus.ItemsSource = mdlMain.dicStatus;
            SetItemSource();
        }
        private void btnVendorNew_Click(object sender, RoutedEventArgs e)
        {
            NewVendorWindow newVendor = new NewVendorWindow();
            newVendor.ShowDialog();
            SetItemSource();
        }

        private void btnVendorDelete_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new RESContext())
            {
                var selectedItem = lvVendor.SelectedItems;
                for (int i = 0; i < selectedItem.Count; i++)
                {
                    Guid guid = Guid.Parse(selectedItem[i].GetType().GetProperties().First(x => x.Name == "ven_uid").GetValue(selectedItem[i], null) + "");
                    var currentVendor = context.Vendors.FirstOrDefault(x => x.ven_uid == guid);
                    currentVendor.status = 1;
                    context.SaveChanges();
                }
            }
            SetItemSource();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource();

        }

        private void cbxStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();

        }

        private void lvVendor_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListView items = sender as ListView;
            Vendor ve = (Vendor)items.SelectedItem;
            if (ve != null)
            {
                DetailVendorWindow detail = new DetailVendorWindow();
                detail.ven_id = ve.ven_uid;
                detail.ShowDialog();
            }
            SetItemSource();
        }

        private void cbxAll_Checked(object sender, RoutedEventArgs e)
        {
            lvVendor.SelectAll();
        }

        private void cbxAll_Unchecked(object sender, RoutedEventArgs e)
        {
            lvVendor.UnselectAll();
        }
    }
}
