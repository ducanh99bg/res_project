﻿using System;
using System.Windows;
using RES.Models;
using RES.Common;
using System.Linq;


namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for NewCategoriesWindow.xaml
    /// </summary>
    public partial class NewCategoriesWindow : Window
    {
        public NewCategoriesWindow()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    Category ca = new Category()
                    {
                        ca_uid = Guid.NewGuid(),
                        name = txtCategoryName.Text + "",
                        description = txtCategoryDescription.Text + "",
                        de_uid = context.Departments.FirstOrDefault(x => x.name.CompareTo(cbxDepartment.Text + "") == 0).de_uid,
                        status = (cbxActive.IsChecked == true) ? (short)2 : (short)1
                    };
                    context.Categories.Add(ca);
                    context.SaveChanges();
                };
            }
            catch(Exception ex)
            {
                MessageBox.Show("Thông tin chưa hợp lệ vui lòng nhập lại");
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            using (var context = new RESContext())
            {
                cbxDepartment.ItemsSource = context.Departments.Where(x => x.status == 2).ToDictionary(x => x.de_uid, x => x.name);
            }
        }
    }
}
