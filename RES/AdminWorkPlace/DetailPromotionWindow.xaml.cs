﻿using RES.Common;
using RES.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DetailPromotionWindow.xaml
    /// </summary>
    public partial class DetailPromotionWindow : Window
    {
        internal Guid id;
        internal Promotion po;
        public DetailPromotionWindow()
        {
            InitializeComponent();
            po = new Promotion();
        }

        public void SetData()
        {
            try
            {
                using (var context = new RESContext())
                {
                    po = context.Promotions.FirstOrDefault(x => x.pro_uid == id);
                    txtTitle.Text = po.title;
                    txtDescription.Text = po.description;
                    startdate.SelectedDate = po.start;
                    enddate.SelectedDate = po.end;
                    timestart.Text = po.timestart.Substring(0, 2) + ":" + po.timestart.Substring(2);
                    timeend.Text = po.timeend.Substring(0, 2) + ":" + po.timeend.Substring(2);
                    isActive.IsChecked = (po.status == 2) ? true : false;
                }

            }
            catch (Exception ex)
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetData();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    string title = txtTitle.Text + "";
                    string des = txtDescription.Text + "";
                    //string start = timestart.SelectedTime.Value.Hour.ToString("00") + timestart.SelectedTime.Value.Minute.ToString("00");
                    //string end = timeend.SelectedTime.Value.Hour.ToString("00") + timeend.SelectedTime.Value.Minute.ToString("00");
                    string start = Convert.ToInt32(timestart.Text.Substring(0, timestart.Text.IndexOf(':'))).ToString("00") + Convert.ToInt32(timestart.Text.Substring(timestart.Text.IndexOf(':') + 1)).ToString("00");
                    string end = Convert.ToInt32(timeend.Text.Substring(0, timeend.Text.IndexOf(':'))).ToString("00") + Convert.ToInt32(timeend.Text.Substring(timestart.Text.IndexOf(':') + 1)).ToString("00");
                    DateTime fromdate = startdate.SelectedDate.Value;
                    DateTime todate = enddate.SelectedDate.Value;
                    short status = (isActive.IsChecked == true) ? (short)2 : (short)1;

                    var promotion = context.Promotions.FirstOrDefault(x => x.pro_uid == id);
                    promotion.title = title;
                    promotion.description = des;
                    promotion.start = fromdate;
                    promotion.end = todate;
                    promotion.status = status;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDiscountBill_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            DiscountTotalBillWindow totalDiscount = new DiscountTotalBillWindow();
            totalDiscount.promotion_uid = id;
            totalDiscount.ShowDialog();
            this.ShowDialog();
        }

        private void btnDiscountProduct_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            DiscountProductWindow productDiscount = new DiscountProductWindow();
            productDiscount.promotion_uid = id;
            productDiscount.ShowDialog();
            this.ShowDialog();
        }
    }
}
