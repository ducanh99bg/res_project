﻿using RES.Models;
using System;
using System.Windows;


namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for NewVendorWindow.xaml
    /// </summary>
    public partial class NewVendorWindow : Window
    {
        public NewVendorWindow()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    Vendor ven = new Vendor()
                    {
                        ven_uid = Guid.NewGuid(),
                        name = txtName.Text + "",
                        address = txtAddress.Text + "",
                        phone = txtPhone.Text + "",
                        email = txtEmail.Text + "",
                        status = (isActive.IsChecked == true) ? 2 : 1
                    };
                    context.Vendors.Add(ven);
                    context.SaveChanges();
                }
            }
            catch(Exception ex)
            {
            }
            this.Close();
        }
    }
}
