﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for ProductPage.xaml
    /// </summary>
    public partial class ProductPage : Page
    {
        internal List<Product> lstPro;
        public ProductPage()
        {
            InitializeComponent();
            lstPro = new List<Product>();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            cbxDepartment.ItemsSource = mdlMain.getDepartment();
            //cbxCategory.ItemsSource = mdlMain.getCategory().Where(x => x.Key != 0);
            cbxStatus.ItemsSource = mdlMain.dicStatus;
            cbxForSale.ItemsSource = mdlMain.dicForSale;
            SetItemSource();
        }
        private void cbxDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (var context = new RESContext())
            {
                if(cbxDepartment.SelectedIndex == 0)
                {
                    cbxCategory.ItemsSource = new Dictionary<int,string>();
                }
                else
                {
                    string dename = mdlMain.dicDe[cbxDepartment.SelectedIndex];
                    Guid decode = context.Departments.FirstOrDefault(x => x.name == dename).de_uid;
                    cbxCategory.ItemsSource = mdlMain.getCategoryByDepartment(decode);
                }

            };
            SetItemSource();

        }
        private void SetItemSource()
        {
            try
            {
                string search = txtSearch.Text + "";
                int status = cbxStatus.SelectedIndex;
                int department = cbxDepartment.SelectedIndex;
                int category = cbxCategory.SelectedIndex;
                int forsale = cbxForSale.SelectedIndex;

                using (var context = new RESContext())
                {
                    lstPro = context.Products.ToList();
                    string dename = "";
                    Guid decode = new Guid();


                    if (department != 0)
                    {
                        dename = mdlMain.dicDe[cbxDepartment.SelectedIndex];
                        decode = context.Departments.FirstOrDefault(x => x.name == dename).de_uid;
                        List<Category> cate = new List<Category>();
                        cate = context.Categories.Where(x => x.de_uid == decode).ToList();
                        lstPro = lstPro.Where(x => cate.Contains(context.Categories.FirstOrDefault(y => y.ca_uid == x.ca_uid))).ToList();
                    }
                    if (category >= 0)
                    {
                        string caname = mdlMain.getCategoryByDepartment(decode)[cbxCategory.SelectedIndex];
                        Guid cacode = context.Categories.FirstOrDefault(x => x.name == caname).ca_uid;
                        lstPro = lstPro.Where(x => x.ca_uid == cacode).ToList();
                    }
                    if (status != 0)
                    {
                        lstPro = lstPro.Where(x => x.status == status).ToList();
                    }
                    if (!string.IsNullOrEmpty(search))
                    {
                        lstPro = lstPro.Where(x => x.name.Contains(search)).ToList();
                    }
                    if(forsale != 0)
                    {
                        lstPro = lstPro.Where(x => x.forsale == forsale).ToList();
                    }

                    lvProduct.ItemsSource = lstPro;
                }
            }
            catch(Exception ex)
            {

            }

        }


        private void btnProductNew_Click(object sender, RoutedEventArgs e)
        {
            NewProductWindow newProduct = new NewProductWindow();
            newProduct.ShowDialog();
            SetItemSource();
        }

        private void cbxStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxProductAll_Unchecked(object sender, RoutedEventArgs e)
        {
            lvProduct.UnselectAll();
        }
        private void cbxForSale_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }
        private void cbxProductAll_Checked(object sender, RoutedEventArgs e)
        {
            lvProduct.SelectAll();
        }

        private void btnProductDelete_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new RESContext())
            {
                var selectedItem = lvProduct.SelectedItems;
                for (int i = 0; i < selectedItem.Count; i++)
                {
                    Guid guid = Guid.Parse(selectedItem[i].GetType().GetProperties().First(x => x.Name == "product_uid").GetValue(selectedItem[i], null) + "");
                    var currentProduct = context.Products.FirstOrDefault(x => x.product_uid == guid);
                    currentProduct.status = 1;
                    context.SaveChanges();
                }
            }
            SetItemSource();
        }

        private void lvProduct_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                ListView items = sender as ListView;
                Product pro = (Product)items.SelectedItem;
                if (pro != null)
                {
                    DetailProductWindow de = new DetailProductWindow();
                    de.productId = pro.product_uid;
                    de.ShowDialog();
                }
            }
            catch(Exception ex)
            {

            }
            SetItemSource();
        }

        
    }
}
