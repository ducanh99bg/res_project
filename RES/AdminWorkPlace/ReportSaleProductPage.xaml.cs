﻿using System;
using System.Collections.Generic;
using System.Linq;
using RES.Common;
using RES.Models;
using System.Windows.Controls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for ReportSaleProductPage.xaml
    /// </summary>
    public partial class ReportSaleProductPage : Page
    {
        public class ReportSaleProduct
        {
            public Guid id { get; set; }
            public Guid pro_uid { get; set; }
            public string pro_name { get; set; }
            public string de_name { get; set; }
            public string ca_name { get; set; }
            public Guid register_uid { get; set; }
            public string register_name { get; set; }
            public string emp_name { get; set; }
            public DateTime time { get; set; }
            public string sale_amount { get; set; }
            public string price { get; set; }
            public string total { get; set; }
        }

        internal List<ReportSaleProduct> listReportProduct;
        public ReportSaleProductPage()
        {
            InitializeComponent();
        }

        public void SetItemSource()
        {
            try
            {
                listReportProduct = new List<ReportSaleProduct>();
                string search = txtSearch.Text + "";
                int department = cbxDepartment.SelectedIndex;
                int category = cbxCategory.SelectedIndex;
                DateTime start = dptStart.SelectedDate.Value.Date.AddHours(0).AddMinutes(0);
                DateTime end = dptEnd.SelectedDate.Value.Date.AddHours(23).AddMinutes(59);
                //Guid emp 
                using (var context = new RESContext())
                {

                    List<Order> orders = context.Orders.Where(x => x.created >= start && x.created <= end).ToList();
                    //
                    foreach(Order item in orders)
                    {
                        List<OrderDetail> details = new List<OrderDetail>();
                        details = context.OrderDetails.Where(x => x.or_uid == item.or_uid).ToList();
                        foreach(OrderDetail o in details)
                        {
                            ReportSaleProduct re = new ReportSaleProduct()
                            {
                                id = Guid.NewGuid(),
                                pro_uid = o.po_uid,
                                pro_name = context.Products.FirstOrDefault(x => x.product_uid == o.po_uid).name.ToString(),
                                de_name = context.Departments.FirstOrDefault(x => x.de_uid == context.Categories.FirstOrDefault(y => y.ca_uid == context.Products.FirstOrDefault(z => z.product_uid == o.po_uid).ca_uid).de_uid).name.ToString(),
                                ca_name = context.Categories.FirstOrDefault(x => x.ca_uid == context.Products.FirstOrDefault(y => y.product_uid == o.po_uid).ca_uid).name.ToString(),
                                register_uid = item.re_uid,
                                register_name = context.Registers.FirstOrDefault(x => x.re_uid == item.re_uid).name.ToString(),
                                emp_name = context.Employees.FirstOrDefault(x => x.emp_uid == context.Registers.FirstOrDefault(y => y.re_uid == item.re_uid).emp_uid).lastname.ToString(),
                                time = item.created,
                                sale_amount = o.quatity.ToString(),
                                price = o.price.ToString("###,###,###") + " VND",
                                total = (o.quatity * o.price).ToString("###,###,###") + " VND",
                            };
                            listReportProduct.Add(re);
                        }
                    }

                    if (!string.IsNullOrEmpty(search))
                    {
                        listReportProduct = listReportProduct.Where(x => x.ca_name.Contains(search) == true).ToList();
                    }
                    if (department > 0)
                    {
                        string name = mdlMain.getDepartment()[department].ToString();
                        listReportProduct = listReportProduct.Where(x => x.de_name.CompareTo(name) == 0).ToList();
                    }
                    if (category >= 0)
                    {
                        string name = mdlMain.getDepartment()[department].ToString();
                        Guid decode = context.Departments.FirstOrDefault(x => x.name == name).de_uid;
                        string caname = mdlMain.getCategoryByDepartment(decode)[category].ToString();
                        listReportProduct = listReportProduct.Where(x => x.ca_name.CompareTo(caname) == 0).ToList();
                    }
                }
                lvProductSale.ItemsSource = listReportProduct.OrderByDescending(x => x.time).ToList();
            }
            catch(Exception ex) { 

            }
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            cbxDepartment.ItemsSource = mdlMain.getDepartment();
            dptStart.SelectedDate = DateTime.Now.Date;
            dptEnd.SelectedDate = DateTime.Now.Date;
            SetItemSource();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource();
        }
        public void PdfFile()
        {
            try
            {
                string fontPath = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "\\Font\\ARIALUNI.TTF";
                Font f = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                f.Size = 10;
                f.SetStyle(Font.NORMAL);

                Font title = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                title.Size = 20;
                title.SetStyle(Font.NORMAL);

                Document pdfDoc = new Document(PageSize.A4.Rotate(), 20f, 20f, 40f, 40f);
                string path = $"E:\\bill\\report1.pdf";
                PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.Create));
                pdfDoc.Open();
                pdfDoc.AddTitle("Thống kê bán hàng");
                pdfDoc.Add(new Paragraph("Thống kê chi tiết bán hàng", title)
                {
                    SpacingBefore = 40f,
                    SpacingAfter = 40f,
                    Alignment = Element.ALIGN_CENTER
                });
                var headerTable = new PdfPTable(new[] { 2f, 2f})
                {
                    WidthPercentage = 75,
                    DefaultCell = { MinimumHeight = 22f },

                };

                var startDate = new Paragraph("Từ ngày: " + dptStart.SelectedDate.Value.ToString("dd-MM-yyyy"), f);
                var endDate = new Paragraph("Tới ngày: " + dptEnd.SelectedDate.Value.ToString("dd-MM-yyyy"), f);

                headerTable.DefaultCell.Border = Rectangle.NO_BORDER;
                headerTable.AddCell(startDate);
                headerTable.AddCell(endDate);
                pdfDoc.Add(headerTable);

                //Table 1
                var columnWidths = new[] { 1f, 1f, 1f, 0.75f, 1.25f, 1f, 1f, 1f, 1f};
                var table = new PdfPTable(columnWidths)
                {
                    WidthPercentage = 100,
                    DefaultCell = { MinimumHeight = 22f }

                };

                var product = new Paragraph("Món ăn", f);
                var department = new Paragraph("Danh mục", f);
                var category = new Paragraph("Loại", f);
                var register = new Paragraph("Ca làm việc", f);
                var time = new Paragraph("Thời gian", f);
                var employee = new Paragraph("Nhân viên", f);
                var price = new Paragraph("Đơn giá", f);
                var quatity = new Paragraph("Số lượng bán ra", f);
                var total = new Paragraph("Thành tiền", f);

                table.DefaultCell.Border = Rectangle.BOTTOM_BORDER;

                table.AddCell(product);
                table.AddCell(department);
                table.AddCell(category);
                table.AddCell(register);
                table.AddCell(time);
                table.AddCell(employee);
                table.AddCell(price);
                table.AddCell(quatity);
                table.AddCell(total);


                using (var context = new RESContext())
                {
                    foreach (ReportSaleProduct item in listReportProduct)
                    {
                        var productItem = new Paragraph(item.pro_name, f);
                        var departmentItem = new Paragraph(item.de_name, f);
                        var categoryItem = new Paragraph(item.ca_name.ToString(), f);
                        var registerItem = new Paragraph(item.register_name.ToString(), f);
                        var timeItem = new Paragraph(item.time.ToString("dd-MM-yyyy HH:mm"), f);
                        var employeeItem = new Paragraph(item.emp_name.ToString(), f);
                        var priceItem = new Paragraph(item.price.ToString(), f);
                        var quatityItem = new Paragraph(item.sale_amount.ToString(), f);
                        var totalItem = new Paragraph(item.total, f);

                        table.AddCell(productItem);
                        table.AddCell(departmentItem);
                        table.AddCell(categoryItem);
                        table.AddCell(registerItem);
                        table.AddCell(timeItem);
                        table.AddCell(employeeItem);
                        table.AddCell(priceItem);
                        table.AddCell(quatityItem);
                        table.AddCell(totalItem);
                    }
                }
                pdfDoc.Add(table);
                pdfDoc.Close();
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {

            }
        }
        private void btnExport_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PdfFile();
        }

        private void cbxDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (var context = new RESContext())
            {
                if (cbxDepartment.SelectedIndex == 0)
                {
                    cbxCategory.ItemsSource = new Dictionary<int, string>();
                }
                else
                {
                    string dename = mdlMain.dicDe[cbxDepartment.SelectedIndex];
                    Guid decode = context.Departments.FirstOrDefault(x => x.name == dename).de_uid;
                    cbxCategory.ItemsSource = mdlMain.getCategoryByDepartment(decode);
                }

            };
            SetItemSource();
        }

        private void cbxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxRegister_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cbxEmployee_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dptStart_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void dptEnd_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }
    }
}
