﻿using System;
using System.Collections.Generic;
using System.Linq;
using RES.Models;
using System.Windows.Controls;
using RES.Common;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for CustomerPage.xaml
    /// </summary>
    public partial class CustomerPage : Page
    {
        public class CustomerRevenue
        {
            public Guid cus_uid { get; set; }
            public string cus_name { get; set; }
            public string address { get; set; }
            public string phone { get; set; }
            public int order_amount { get; set; }
            public int order_noncash { get; set; }
            public double cash_amount { get; set; }
            public double cash_non { get; set; }
        }
        List<CustomerRevenue> listCustomer;
        public CustomerPage()
        {
            InitializeComponent();
        }

        public void SetItemSource()
        {
            try
            {
                listCustomer = new List<CustomerRevenue>();
                string search = txtSearch.Text + "";
                int status = cbxStatus.SelectedIndex;
                using (var context = new RESContext())
                {
                    foreach (Customer cus in context.Customers.ToList())
                    {
                        int amount = 0;
                        int noncash = 0;
                        double cash = 0;
                        double non = 0;
                        foreach(Order order in context.Orders.Where(x =>x.cus_uid == cus.cus_uid).ToList())
                        {
                            if(order.status == 1)
                            {
                                amount++;
                                cash += order.total;
                            }
                            else
                            {
                                noncash++;
                                non += order.total;
                            }
                            

                        }
                        CustomerRevenue cu = new CustomerRevenue()
                        {
                            cus_uid = cus.cus_uid,
                            cus_name = cus.firstname + " " + cus.lastname,
                            address = cus.address,
                            phone = cus.phone,
                            order_amount = amount,
                            order_noncash = noncash,
                            cash_amount = cash,
                            cash_non = non
                        };
                        listCustomer.Add(cu);
                    }
                }
                if (!string.IsNullOrEmpty(search))
                {
                    listCustomer = listCustomer.Where(x => x.cus_name.Contains(search)).ToList();
                }
                if(status == 1)
                {
                    listCustomer = listCustomer.Where(x => x.cash_non != 0).ToList();
                }
                if(status == 2)
                {
                    listCustomer = listCustomer.Where(x => x.cash_non == 0).ToList();
                }
                lvCustomer.ItemsSource = listCustomer.OrderByDescending(x => x.order_amount).ToList();
            }
            catch(Exception ex)
            {

            }
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            cbxStatus.ItemsSource = mdlMain.dicCustomerStatus;
            SetItemSource();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }
    }
}
