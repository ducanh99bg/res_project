﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using RES.Common;
using RES.Models;
using System.IO;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for NewEmpWindow.xaml
    /// </summary>
    public partial class NewEmpWindow : Window
    {
        private string imgPath = "";
        public NewEmpWindow()
        {
            InitializeComponent();
        }

        //Image Browser
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "c:\\Users\\chudu\\source\\repos\\RES\\RES\\img\\employee";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            if(dialog.ShowDialog() == true)
            {
                string selectedFileName = dialog.FileName;
                //Copy to new folder
                string filename = Path.GetFileName(selectedFileName);
                var environment = Environment.CurrentDirectory;
                string dicPath = Directory.GetParent(environment).Parent.FullName + "\\img\\employee\\" + filename;
                if (!File.Exists(dicPath))
                {
                    File.Copy(selectedFileName, dicPath);
                }
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Directory.GetParent(environment).Parent.FullName + "\\img\\employee\\" + filename);
                bitmap.EndInit();
                imgViewer.Source = bitmap;

                imgPath = "\\img\\employee\\" + filename;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Set role
            cbxPosition.ItemsSource = mdlMain.dicPosition.Where(x => x.Key != 0);
        }
        //close window
        private void btnCacel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            using(var context = new RESContext())
            {
                string firstname = txtFirstName.Text + "";
                string lastname = txtLastName.Text + "";
                int gender = (rdFeMale.IsChecked == true) ? 1 : 0;
                DateTime birth = dptBirth.SelectedDate.Value;
                string address = txtAddress.Text + "";
                string phone = txtPhone.Text + "";
                string img = imgPath;
                string password = txtPassWord.Password + "";
                string confirmPassWord = txtConfirmPass.Password + "";
                if(password.CompareTo(confirmPassWord) != 0)
                {
                    MessageBox.Show("Mật khẩu nhập lại chưa đúng !");
                }
                int position = Convert.ToInt32(cbxPosition.SelectedValue + "");
                short status = (cbxEmpActive.IsChecked == true) ? (short)2 : (short)1;

                DateTime createdDate = DateTime.Now;

                //Create a new acount when create a new employee

                Account ac = new Account()
                {
                    ac_uid = Guid.NewGuid(),
                    username = lastname,
                    password = password,
                    role = position,
                    created = createdDate,
                    modified = createdDate,
                    status = status
                };
                context.Accounts.Add(ac);

                //Create a new employee
                Employee emp = new Employee()
                {
                    emp_uid = Guid.NewGuid(),
                    ac_uid = ac.ac_uid,
                    firstname = firstname,
                    lastname = lastname,
                    gender = (short)gender,
                    birthdate = birth,
                    address = address,
                    phone = phone,
                    startdate = createdDate,
                    created = createdDate,
                    modified = createdDate,
                    img = imgPath,
                    status = status,
                    Account = ac
                };
                context.Employees.Add(emp);
                context.SaveChanges();
                this.Close();
            }
        }
    }
}
