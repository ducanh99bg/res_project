﻿using Microsoft.Win32;
using RES.Common;
using RES.Models;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DetailInventoryWindow.xaml
    /// </summary>
    public partial class DetailInventoryWindow : Window
    {
        internal string imgPath = "";
        internal Guid inv_uid;
        internal Inventory inv;
        public DetailInventoryWindow()
        {
            InitializeComponent();
        }
        public void SetData()
        {
            try
            {
                using(var context = new RESContext())
                {
                    inv = context.Inventorys.FirstOrDefault(x => x.inv_uid == inv_uid);
                    txtName.Text = inv.name + "";
                    txtCost.Text = inv.cost + "";
                    txtQuatity.Text = inv.quatity + "";
                    isActive.IsChecked = (inv.status == 2) ? true : false;

                    var environment = Environment.CurrentDirectory;
                    string dicPath = Directory.GetParent(environment).Parent.FullName + inv.img.ToString();
                    if (File.Exists(dicPath))
                    {
                        BitmapImage bitmap = new BitmapImage();
                        bitmap.BeginInit();
                        bitmap.UriSource = new Uri(Directory.GetParent(environment).Parent.FullName + inv.img);
                        bitmap.EndInit();
                        imgViewer.Source = bitmap;
                        imgPath = inv.img;
                    }

                    Guid vendor = inv.ven_uid;
                    string ven_name = "";
                    ven_name = context.Vendors.FirstOrDefault(x => x.ven_uid == vendor).name;
                    cbxVendor.SelectedIndex = mdlMain.getVendor().FirstOrDefault(x => x.Value == ven_name).Key - 1;

                    cbxType.SelectedIndex = inv.type - 1;

                }
            }
            catch(Exception ex)
            {

            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbxType.ItemsSource = mdlMain.dicInventoryType.Where(x => x.Key != 0);
            cbxStockUnit.ItemsSource = mdlMain.dicStockUnit.Where(x => x.Key != 0);
            cbxVendor.ItemsSource = mdlMain.getVendor().Where(x => x.Key != 0);
            SetData();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    string name = txtName.Text + "";
                    int type = cbxType.SelectedIndex + 1;
                    string img = imgPath;
                    double cost = 0;
                    double.TryParse(txtCost.Text + "", out cost);
                    double quatity = 0;
                    double.TryParse(txtQuatity.Text + "", out quatity);
                    int stock = cbxStockUnit.SelectedIndex + 1;
                    int status = (isActive.IsChecked == true) ? 2 : 1;

                    string vendor = mdlMain.getVendor()[cbxVendor.SelectedIndex + 1];
                    Guid venId = context.Vendors.FirstOrDefault(x => x.name == vendor).ven_uid;
                    Vendor ven = context.Vendors.FirstOrDefault(x => x.ven_uid == venId);
                    Inventory inven = context.Inventorys.FirstOrDefault(x => x.inv_uid == inv_uid);
                    inven.name = name;
                    inven.type = type;
                    inven.quatity = quatity;
                    inven.img = img;
                    inven.cost = cost;
                    inven.stockunit = stock;
                    inven.status = status;
                    inven.ven_uid = venId;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Dữ liệu chưa đúng! Thao tác không thể thực hiện");
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "c:\\";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            if (dialog.ShowDialog() == true)
            {
                string selectedFileName = dialog.FileName;
                //Copy to new folder
                string filename = System.IO.Path.GetFileName(selectedFileName);
                var environment = Environment.CurrentDirectory;
                string dicPath = Directory.GetParent(environment).Parent.FullName + "\\img\\inventory\\" + filename;
                if (!File.Exists(dicPath))
                {
                    File.Copy(selectedFileName, dicPath);
                }
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Directory.GetParent(environment).Parent.FullName + "\\img\\inventory\\" + filename);
                bitmap.EndInit();
                imgViewer.Source = bitmap;

                imgPath = "\\img\\inventory\\" + filename;
            }
        }
    }
}
