﻿using System;
using System.Collections.Generic;
using System.Linq;
using RES.Common;
using RES.Models;
using System.Windows.Controls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for ReportProductPage.xaml
    /// </summary>
    public partial class ReportProductPage : Page
    {
        public class ReportProduct
        {
            public Guid id { get; set; }
            public Guid pro_uid { get; set; }
            public string pro_name { get; set; }
            public string de_name { get; set; }
            public string ca_name { get; set; }
            public double sale_amount { get; set; }
            public double price { get; set; }
            public double total { get; set; }

        }

        internal List<ReportProduct> listProduct;
        public ReportProductPage()
        {
            InitializeComponent();
        }
        public void SetItemSource()
        {
            try
            {
                listProduct = new List<ReportProduct>();
                string search = txtSearch.Text + "";
                int department = cbxDepartment.SelectedIndex;
                int category = cbxCategory.SelectedIndex;
                int type = cbxType.SelectedIndex;
                DateTime start = dptStart.SelectedDate.Value.Date.AddHours(0).AddMinutes(0);
                DateTime end = dptEnd.SelectedDate.Value.Date.AddHours(23).AddMinutes(59);
                //Guid emp 
                using (var context = new RESContext())
                {
                    List<Product> products = context.Products.Where(x => x.status == 2).ToList();
                    foreach (Product item in products)
                    {
                        ReportProduct re = new ReportProduct()
                        {
                            id = Guid.NewGuid(),
                            pro_uid = item.product_uid,
                            pro_name = item.name,
                            de_name = context.Departments.FirstOrDefault(x => x.de_uid == context.Categories.FirstOrDefault(y => y.ca_uid == context.Products.FirstOrDefault(z => z.product_uid == item.product_uid).ca_uid).de_uid).name.ToString(),
                            ca_name = context.Categories.FirstOrDefault(x => x.ca_uid == context.Products.FirstOrDefault(y => y.product_uid == item.product_uid).ca_uid).name.ToString(),
                            sale_amount = 0,
                            price = item.price,
                        };

                        foreach(OrderDetail detail in context.OrderDetails.Where(x => x.po_uid == item.product_uid).ToList())
                            {
                            Order od = context.Orders.FirstOrDefault(x => x.or_uid == detail.or_uid && x.created >= start && x.created <= end);
                            if (od != null)
                            {
                                re.sale_amount += detail.quatity;
                                re.total += (detail.price * detail.quatity);
                            }
                        }
                        listProduct.Add(re);
                    }

                    if (!string.IsNullOrEmpty(search))
                    {
                        listProduct = listProduct.Where(x => x.pro_name.Contains(search) == true).ToList();
                    }
                    if (department > 0)
                    {
                        string name = mdlMain.getDepartment()[department].ToString();
                        listProduct = listProduct.Where(x => x.de_name.CompareTo(name) == 0).ToList();
                    }
                    if (category >= 0)
                    {
                        string name = mdlMain.getDepartment()[department].ToString();
                        Guid decode = context.Departments.FirstOrDefault(x => x.name == name).de_uid;
                        string caname = mdlMain.getCategoryByDepartment(decode)[category].ToString();
                        listProduct = listProduct.Where(x => x.ca_name.CompareTo(caname) == 0).ToList();
                    }
                }
                if(type == 0)
                {
                    lvProductSale.ItemsSource = listProduct.OrderByDescending(x => x.sale_amount).ToList();
                }
                else
                {
                    lvProductSale.ItemsSource = listProduct.OrderByDescending(x => x.total).ToList();
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            cbxDepartment.ItemsSource = mdlMain.getDepartment();
            cbxType.ItemsSource = mdlMain.dicReportProduct;
            dptStart.SelectedDate = DateTime.Now.Date;
            dptEnd.SelectedDate = DateTime.Now.Date;
            SetItemSource();
        }

        private void dptStart_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void dptEnd_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }
        public void PdfFile()
        {
            try
            {
                string fontPath = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "\\Font\\ARIALUNI.TTF";
                Font f = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                f.Size = 10;
                f.SetStyle(Font.NORMAL);

                Font title = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                title.Size = 20;
                title.SetStyle(Font.NORMAL);

                Document pdfDoc = new Document(PageSize.A4.Rotate(), 20f, 20f, 40f, 40f);
                string path = $"E:\\bill\\report3.pdf";
                PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.Create));
                pdfDoc.Open();
                pdfDoc.AddTitle("Thống kê bán hàng");
                pdfDoc.Add(new Paragraph("Thống kê doanh số theo sản phẩm", title)
                {
                    SpacingBefore = 40f,
                    SpacingAfter = 40f,
                    Alignment = Element.ALIGN_CENTER
                });
                var headerTable = new PdfPTable(new[] { 2f, 2f })
                {
                    WidthPercentage = 75,
                    DefaultCell = { MinimumHeight = 22f },

                };

                var startDate = new Paragraph("Từ ngày: " + dptStart.SelectedDate.Value.ToString("dd-MM-yyyy"), f);
                var endDate = new Paragraph("Tới ngày: " + dptEnd.SelectedDate.Value.ToString("dd-MM-yyyy"), f);

                headerTable.DefaultCell.Border = Rectangle.NO_BORDER;
                headerTable.AddCell(startDate);
                headerTable.AddCell(endDate);
                pdfDoc.Add(headerTable);

                //Table 1
                var columnWidths = new[] { 1f, 1f, 1f, 0.75f, 1.25f, 1f};
                var table = new PdfPTable(columnWidths)
                {
                    WidthPercentage = 100,
                    DefaultCell = { MinimumHeight = 22f }

                };

                var product = new Paragraph("Sản phẩm", f);
                var department = new Paragraph("Danh mục", f);
                var category = new Paragraph("Loại", f);
                var price = new Paragraph("Giá bán hiện tại", f);
                var quatity = new Paragraph("Số lượng bán ra", f);
                var total = new Paragraph("Thành tiền", f);

                table.DefaultCell.Border = Rectangle.BOTTOM_BORDER;

                table.AddCell(product);
                table.AddCell(department);
                table.AddCell(category);
                table.AddCell(price);
                table.AddCell(quatity);
                table.AddCell(total);


                using (var context = new RESContext())
                {
                    foreach (ReportProduct item in listProduct)
                    {
                        var productItem = new Paragraph(item.pro_name, f);
                        var departmentItem = new Paragraph(item.de_name, f);
                        var categoryItem = new Paragraph(item.ca_name.ToString(), f);
                        var priceItem = new Paragraph(item.price.ToString("###,###,###,###") + " VND", f);
                        var quatityItem = new Paragraph(item.sale_amount.ToString(), f);
                        var totalItem = new Paragraph(item.total.ToString("###,###,###,###") + " VND", f);

                        table.AddCell(productItem);
                        table.AddCell(departmentItem);
                        table.AddCell(categoryItem);
                        table.AddCell(priceItem);
                        table.AddCell(quatityItem);
                        table.AddCell(totalItem);
                    }
                }
                pdfDoc.Add(table);
                pdfDoc.Close();
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {

            }
        }
        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            PdfFile();
        }

        private void cbxDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (var context = new RESContext())
            {
                if (cbxDepartment.SelectedIndex == 0)
                {
                    cbxCategory.ItemsSource = new Dictionary<int, string>();
                }
                else
                {
                    string dename = mdlMain.dicDe[cbxDepartment.SelectedIndex];
                    Guid decode = context.Departments.FirstOrDefault(x => x.name == dename).de_uid;
                    cbxCategory.ItemsSource = mdlMain.getCategoryByDepartment(decode);
                }

            };
            SetItemSource();
        }

        private void cbxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource();
        }
    }
}
