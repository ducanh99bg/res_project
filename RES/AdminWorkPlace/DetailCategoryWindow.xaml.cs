﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DetailCategoryWindow.xaml
    /// </summary>
    public partial class DetailCategoryWindow : Window
    {
        internal Guid cate_uid;
        internal Category cate;
        public DetailCategoryWindow()
        {
            InitializeComponent();
        }
        private void SetData()
        {
            using (var context = new RESContext())
            {
                cate = context.Categories.FirstOrDefault(x => x.ca_uid == cate_uid);
                txtCategoryName.Text = cate.name + "";
                txtCategoryDescription.Text = cate.description + "";

                string dename = "";
                dename = context.Departments.FirstOrDefault(x => x.de_uid == cate.de_uid).name;
                cbxDepartment.SelectedIndex = mdlMain.getDepartment().FirstOrDefault(x => x.Value.CompareTo(dename) == 0).Key - 1;

                cbxActive.IsChecked = (cate.status == 2) ? true : false;
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbxDepartment.ItemsSource = mdlMain.getDepartment().Where(x => x.Key != 0);
            SetData();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    string name = txtCategoryName.Text + "";
                    string description = txtCategoryDescription.Text + "";
                    string dename = mdlMain.getDepartment()[cbxDepartment.SelectedIndex + 1];
                    Guid department = context.Departments.FirstOrDefault(x => x.name.CompareTo(dename) == 0).de_uid;
                    short status = (cbxActive.IsChecked == true) ? (short)2 : (short)1;

                    try
                    {
                        Category currentCa = context.Categories.SingleOrDefault(x => x.ca_uid == cate_uid);
                        currentCa.name = name;
                        currentCa.description = description;
                        currentCa.de_uid = department;
                        currentCa.status = status;
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Thông tin chưa hợp lệ vui lòng nhập lại");
                    }
                }
            }
            catch(Exception ex)
            {
            }
            this.Close();
        }
    }
}
