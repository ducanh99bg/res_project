﻿using RES.Common;
using RES.Models;
using System;
using System.Windows;


namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for NewPromotionWindow.xaml
    /// </summary>
    public partial class NewPromotionWindow : Window
    {
        public NewPromotionWindow()
        {
            InitializeComponent();
        }


        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    string title = txtTitle.Text + "";
                    string des = txtDescription.Text + "";
                    string start = timestart.SelectedTime.Value.Hour.ToString("00") + timestart.SelectedTime.Value.Minute.ToString("00");
                    string end = timeend.SelectedTime.Value.Hour.ToString("00") + timeend.SelectedTime.Value.Minute.ToString("00");
                    DateTime fromdate = startdate.SelectedDate.Value;
                    DateTime todate = enddate.SelectedDate.Value;

                    Promotion promotion = new Promotion()
                    {
                        pro_uid = Guid.NewGuid(),
                        title = title,
                        description = des,
                        timestart = start,
                        timeend = end,
                        start = fromdate,
                        end = todate,
                        status = 2
                    };

                    context.Promotions.Add(promotion);
                    context.SaveChanges();
                }
            }
            catch(Exception ex)
            {

            }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

    }
}
