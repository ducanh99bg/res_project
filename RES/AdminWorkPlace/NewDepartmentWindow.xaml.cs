﻿using System;
using System.Windows;
using RES.Models;
using RES.Common;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for NewDepartmentWindow.xaml
    /// </summary>
    public partial class NewDepartmentWindow : Window
    {
        public NewDepartmentWindow()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    string name = txtDepartmentName.Text + "".Trim();
                    string description = txtDescription.Text + "".Trim();
                    short status = (cbxActive.IsChecked == true) ? (short)2 : (short)1;

                    Department de = new Department()
                    {
                        de_uid = Guid.NewGuid(),
                        name = name,
                        description = description,
                        status = status
                    };
                    context.Departments.Add(de);
                    context.SaveChanges();
                    this.Close();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Thông tin chưa hợp lệ vui lòng nhập lại");
                this.Close();
            }
        }
    }
}
