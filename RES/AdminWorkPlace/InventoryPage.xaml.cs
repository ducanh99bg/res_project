﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for InventoryPage.xaml
    /// </summary>
    public partial class InventoryPage : Page
    {
        internal List<Inventory> lstInven;
        public InventoryPage()
        {
            InitializeComponent();
            lstInven = new List<Inventory>();
        }

        public void SetItemSource()
        {
            string search = txtSearch.Text + "";
            int vendor = cbxVendor.SelectedIndex;
            int status = cbxStatus.SelectedIndex;
            int quatitystatus = cbxQuatityStatus.SelectedIndex;
            try
            {
                using(var context = new RESContext())
                {
                    lstInven = context.Inventorys.ToList();
                    if (!string.IsNullOrEmpty(search))
                    {
                        lstInven = lstInven.Where(x => x.name.Contains(search)).ToList();
                    }
                    if(status != 0)
                    {
                        lstInven = lstInven.Where(x => x.status == status).ToList();
                    }
                    if(vendor != 0)
                    {
                        string vendorname = mdlMain.getVendor()[cbxVendor.SelectedIndex];
                        Guid venid = context.Vendors.FirstOrDefault(x => x.name == vendorname).ven_uid;
                        lstInven = lstInven.Where(x => x.ven_uid == venid).ToList();
                    }
                    if(quatitystatus != 0)
                    {
                        lstInven = (quatitystatus == 1) ? lstInven.Where(x => x.quatity > 0).ToList() : lstInven.Where(x => x.quatity == 0).ToList();
                    }
                }
                lvInventory.ItemsSource = lstInven;
            }
            catch(Exception ex) { 
            
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            cbxStatus.ItemsSource = mdlMain.dicStatus;
            cbxVendor.ItemsSource = mdlMain.getVendor();
            cbxQuatityStatus.ItemsSource = mdlMain.dicQuatityStatus;
            SetItemSource();
        }

        private void btnInventoryNew_Click(object sender, RoutedEventArgs e)
        {
            NewInventoryWindow inven = new NewInventoryWindow();
            inven.ShowDialog();
            SetItemSource();
        }

        private void btnInventoryDelete_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new RESContext())
            {
                var selectedItem = lvInventory.SelectedItems;
                for (int i = 0; i < selectedItem.Count; i++)
                {
                    Guid guid = Guid.Parse(selectedItem[i].GetType().GetProperties().First(x => x.Name == "inv_uid").GetValue(selectedItem[i], null) + "");
                    var currentInven = context.Inventorys.FirstOrDefault(x => x.inv_uid == guid);
                    currentInven.status = 1;
                    context.SaveChanges();
                }
            }
            SetItemSource();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxVendor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxQuatityStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void lvInventory_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ListView items = sender as ListView;
                Inventory inventory = (Inventory)items.SelectedItem;
                if (inventory != null)
                {
                    DetailInventoryWindow de = new DetailInventoryWindow();
                    de.inv_uid = inventory.inv_uid;
                    de.ShowDialog();
                }
            }
            catch (Exception ex)
            {

            }
            SetItemSource();
        }

        private void cbxInventoryAll_Unchecked(object sender, RoutedEventArgs e)
        {
            lvInventory.UnselectAll();
        }

        private void cbxInventoryAll_Checked(object sender, RoutedEventArgs e)
        {
            lvInventory.SelectAll();
        }
    }
}
