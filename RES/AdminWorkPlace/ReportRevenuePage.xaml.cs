﻿using System;
using System.Collections.Generic;
using System.Linq;
using RES.Common;
using RES.Models;
using System.Windows.Controls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for ReportRevenuePage.xaml
    /// </summary>
    public partial class ReportRevenuePage : Page
    {
        internal List<Register> registers;

        public ReportRevenuePage()
        {
            InitializeComponent();
        }

        public void SetItemSource()
        {
            try
            {
                DateTime start = dptStart.SelectedDate.Value.Date.AddHours(0).AddMinutes(0);
                DateTime end = dptEnd.SelectedDate.Value.Date.AddHours(23).AddMinutes(59);
                using (var context = new RESContext())
                {
                    registers = context.Registers.Where(x => x.start >= start && x.start <= end).ToList();
                }
                lvRevenue.ItemsSource = registers;
                double sum = 0;
                double.TryParse(registers.Sum(x => x.cashsale)+"", out sum);
                txtTotalRevenue.Text = sum.ToString("###,###,###,###,###") + " VND";
            }
            catch(Exception ex)
            {
            }
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            dptStart.SelectedDate = DateTime.Now.Date;
            dptEnd.SelectedDate = DateTime.Now.Date;
            SetItemSource();
        }

        private void dptStart_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void dptEnd_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }
        public void PdfFile()
        {
            try
            {
                string fontPath = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "\\Font\\ARIALUNI.TTF";
                Font f = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                f.Size = 10;
                f.SetStyle(Font.NORMAL);

                Font title = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                title.Size = 20;
                title.SetStyle(Font.NORMAL);

                Document pdfDoc = new Document(PageSize.A4.Rotate(), 20f, 20f, 40f, 40f);
                string path = $"E:\\bill\\report3.pdf";
                PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.Create));
                pdfDoc.Open();
                pdfDoc.AddTitle("Thống kê bán hàng");
                pdfDoc.Add(new Paragraph("Thống kê theo ca làm việc", title)
                {
                    SpacingBefore = 40f,
                    SpacingAfter = 40f,
                    Alignment = Element.ALIGN_CENTER
                });
                var headerTable = new PdfPTable(new[] { 2f, 2f, 2f})
                {
                    WidthPercentage = 75,
                    DefaultCell = { MinimumHeight = 22f },

                };

                var startDate = new Paragraph("Từ ngày: " + dptStart.SelectedDate.Value.ToString("dd-MM-yyyy"), f);
                var endDate = new Paragraph("Tới ngày: " + dptEnd.SelectedDate.Value.ToString("dd-MM-yyyy"), f);

                double sum = 0;
                double.TryParse(registers.Sum(x => x.cashsale) + "", out sum);
                var revenue = new Paragraph("Tổng doanh thu: " + sum.ToString("###,###,###,###,###") + " VND", f);

                headerTable.DefaultCell.Border = Rectangle.NO_BORDER;
                headerTable.AddCell(startDate);
                headerTable.AddCell(endDate);
                headerTable.AddCell(revenue);
                pdfDoc.Add(headerTable);

                //Table 1
                var columnWidths = new[] { 1f, 1f, 1f, 0.75f, 1.25f, 1f };
                var table = new PdfPTable(columnWidths)
                {
                    WidthPercentage = 100,
                    DefaultCell = { MinimumHeight = 22f }

                };

                var register = new Paragraph("Ca làm việc", f);
                var start = new Paragraph("Thời gian bắt đầu", f);
                var end = new Paragraph("Thời gian kết thúc", f);
                var opening = new Paragraph("Tổng tiền bắt đầu", f);
                var closing = new Paragraph("Tổng tiền kết thúc", f);
                var total = new Paragraph("Doanh thu", f);

                table.DefaultCell.Border = Rectangle.BOTTOM_BORDER;

                table.AddCell(register);
                table.AddCell(start);
                table.AddCell(end);
                table.AddCell(opening);
                table.AddCell(closing);
                table.AddCell(total);


                using (var context = new RESContext())
                {
                    foreach (Register item in registers)
                    {
                        var registerItem = new Paragraph(item.name, f);
                        var startItem = new Paragraph(item.start.ToString(), f);
                        var endItem = new Paragraph(item.end.ToString(), f);
                        var openingItem = new Paragraph(item.openingamount.ToString("###,###,###,##0") + " VND", f);
                        var closingItem = new Paragraph(item.closeamount.ToString("###,###,###,##0") + " VND", f);
                        var totalItem = new Paragraph(item.cashsale.ToString("###,###,###,##0") + " VND", f);

                        table.AddCell(registerItem);
                        table.AddCell(startItem);
                        table.AddCell(endItem);
                        table.AddCell(openingItem);
                        table.AddCell(closingItem);
                        table.AddCell(totalItem);
                    }
                }
                pdfDoc.Add(table);
                pdfDoc.Close();
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {

            }
        }
        private void btnReport_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PdfFile();
        }
    }
}
