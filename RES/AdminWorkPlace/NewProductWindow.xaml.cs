﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using RES.Common;
using RES.Models;
using System.IO;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for NewProductWindow.xaml
    /// </summary>
    public partial class NewProductWindow : Window
    {
        private string imgPath = "";
        public NewProductWindow()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    string name = txtName.Text + "";
                    string des = txtDescription.Text + "";
                    double price = 0;
                    double.TryParse(txtPrice.Text + "", out price);
                    short status = (cbxActive.IsChecked == true) ? (short)2 : (short)1;
                    string caname = mdlMain.dicCate[cbxCategory.SelectedIndex + 1];
                    Category ca = context.Categories.FirstOrDefault(x => x.name == caname);

                    DateTime createdDate = DateTime.Now;

                    Product po = new Product()
                    {
                        product_uid = Guid.NewGuid(),
                        name = name,
                        description = des,
                        price = price,
                        img = imgPath,
                        created = createdDate,
                        modified = createdDate,
                        status = status,
                        forsale = 1,
                        ca_uid = ca.ca_uid,
                        Category = ca
                    };
                    context.Products.Add(po);
                    context.SaveChanges();
                    this.Close();
                };
            }
            catch (Exception ex)
            {
                this.Close();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "c:\\Users\\chudu\\source\\repos\\RES\\RES\\img\\product";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            if (dialog.ShowDialog() == true)
            {
                string selectedFileName = dialog.FileName;
                //Copy to new folder
                string filename = Path.GetFileName(selectedFileName);
                var environment = Environment.CurrentDirectory;
                string dicPath = Directory.GetParent(environment).Parent.FullName + "\\img\\product\\" + filename;
                if (!File.Exists(dicPath))
                {
                    File.Copy(selectedFileName, dicPath);
                }
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Directory.GetParent(environment).Parent.FullName + "\\img\\product\\" + filename);
                bitmap.EndInit();
                imgViewer.Source = bitmap;

                imgPath = "\\img\\product\\" + filename;
            }
        }

        private void btnCacel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbxCategory.ItemsSource = mdlMain.getCategory().Where(x => x.Key != 0);
        }
    }
}
