﻿using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DiscountTotalBillWindow.xaml
    /// </summary>
    public partial class DiscountTotalBillWindow : Window
    {
        internal Guid promotion_uid;
        internal PromotionDetail promotionDetail;
        public DiscountTotalBillWindow()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    promotionDetail = context.PromotionDetails.FirstOrDefault(x => x.pro_uid == promotion_uid && x.type == 2);
                    if(promotionDetail != null)
                    {
                        txtFrom.Text = promotionDetail.start_value.ToString();
                        txtDiscount.Text = promotionDetail.discount_totalbill.ToString();
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    double fromValue = 0;
                    double.TryParse(txtFrom.Text + "", out fromValue);
                    double discount = 0;
                    double.TryParse(txtDiscount.Text + "", out discount);
                    if(promotionDetail == null)
                    {
                        promotionDetail = new PromotionDetail()
                        {
                            prodetail_uid = Guid.NewGuid(),
                            pro_uid = promotion_uid,
                            type = 2,
                            discount_totalbill = discount,
                            start_value = fromValue
                        };
                        context.PromotionDetails.Add(promotionDetail);
                    }
                    else
                    {
                        var proDetail = context.PromotionDetails.FirstOrDefault(x => x.pro_uid == promotion_uid && x.type == 2);
                        proDetail.start_value = fromValue;
                        proDetail.discount_totalbill = discount;
                    }
                    context.SaveChanges();
                }
                this.Close();
            }
            catch(Exception ex)
            {

            }
        }
    }
}
