﻿using RES.Models;
using System;
using System.Linq;
using System.Windows;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DetailVendorWindow.xaml
    /// </summary>
    public partial class DetailVendorWindow : Window
    {
        internal Guid ven_id;
        public DetailVendorWindow()
        {
            InitializeComponent();
        }

        public void SetData()
        {
            using(var context = new RESContext())
            {
                Vendor ven = context.Vendors.FirstOrDefault(x => x.ven_uid == ven_id);
                txtName.Text = ven.name + "";
                txtAddress.Text = ven.address + "";
                txtEmail.Text = ven.email + "";
                txtPhone.Text = ven.phone + "";
                isActive.IsChecked = (ven.status == 2) ? true : false;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetData();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    var ven = context.Vendors.FirstOrDefault(x => x.ven_uid == ven_id);
                    ven.name = txtName.Text + "";
                    ven.address = txtAddress.Text + "";
                    ven.phone = txtPhone.Text + "";
                    ven.email = txtEmail.Text + "";
                    ven.status = (isActive.IsChecked == true) ? 2 : 1;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
            this.Close();
        }
    }
}
