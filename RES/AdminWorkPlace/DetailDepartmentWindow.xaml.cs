﻿using System;
using System.Windows;
using RES.Models;
using System.Linq;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DetailDepartmentWindow.xaml
    /// </summary>
    public partial class DetailDepartmentWindow : Window
    {
        internal Guid deId;
        internal Department de;
        public DetailDepartmentWindow()
        {
            InitializeComponent();
            de = new Department();
        }
        private void SetData()
        {
            using(var context = new RESContext())
            {
                de = context.Departments.FirstOrDefault(x => x.de_uid == deId);
            }
            txtDepartmentName.Text = de.name + "";
            txtDescription.Text = de.description + "";
            cbxActive.IsChecked = (de.status == 2) ? true : false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetData();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            using(var context = new RESContext())
            {
                string name = txtDepartmentName.Text + "";
                string description = txtDescription.Text + "";
                short status = (cbxActive.IsChecked == true) ? (short)2 : (short)1;

                try
                {
                    Department currentDe = context.Departments.SingleOrDefault(x => x.de_uid == deId);

                    var de = context.Departments.FirstOrDefault(x => x.de_uid == currentDe.de_uid);
                    de.name = name;
                    de.description = description;
                    de.status = status;
                    context.SaveChanges();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Thông tin chưa hợp lệ vui lòng nhập lại");
                }
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
