﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using RES.Models;
using RES.Common;
using Microsoft.Win32;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DetailEmpWindow.xaml
    /// </summary>
    public partial class DetailEmpWindow : Window
    {
        internal Guid empId;
        internal Employee emp;
        internal string strImg;
        public DetailEmpWindow()
        {
            InitializeComponent();
            emp = new Employee();
        }

        private void SetData()
        {
            using (var context = new RESContext())
            {
                emp = context.Employees.FirstOrDefault(x => x.emp_uid == empId);
                emp.Account = context.Accounts.FirstOrDefault(x => x.ac_uid == emp.ac_uid);
            };

            var environment = Environment.CurrentDirectory;
            string dicPath = Directory.GetParent(environment).Parent.FullName + emp.img.ToString();
            if (File.Exists(dicPath))
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Directory.GetParent(environment).Parent.FullName + emp.img);
                bitmap.EndInit();
                imgViewer.Source = bitmap;
                strImg = emp.img;
            }

            txtFirstName.Text = emp.firstname + "";
            txtLastName.Text = emp.lastname + "";
            txtAddress.Text = emp.address + "";
            txtPhone.Text = emp.phone + "";
            rdFeMale.IsChecked = (emp.gender == 1) ? true : false;
            rdMale.IsChecked = (emp.gender == 0) ? true : false;
            dptBirth.SelectedDate = emp.birthdate;
            cbxPosition.SelectedIndex = emp.Account.role - 1;
            txtUserName.Text = emp.Account.username;
            txtPassWord.Password = emp.Account.password;
            txtConfirmPass.Password = emp.Account.password;
            cbxEmpActive.IsChecked = (emp.status == 2) ? true : false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbxPosition.ItemsSource = mdlMain.dicPosition.Where(x => x.Key != 0);
            SetData();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new RESContext())
            {
                string firstname = txtFirstName.Text + "";
                string lastname = txtLastName.Text + "";
                int gender = (rdFeMale.IsChecked == true) ? 1 : 0;
                DateTime birth = dptBirth.SelectedDate.Value;
                string address = txtAddress.Text + "";
                string phone = txtPhone.Text + "";
                string password = txtPassWord.Password + "";
                int position = cbxPosition.SelectedIndex + 1;
                DateTime modifieddate = DateTime.Now;

                string username = txtUserName.Text + "";
                string passWord = txtPassWord.Password + "";
                string confirmPass = txtConfirmPass.Password + "";
                short status = (cbxEmpActive.IsChecked == true) ? (short)2 : (short)1;

                if (passWord.CompareTo(confirmPass) != 0)
                {
                    MessageBox.Show("Confirm Pass is not correct !");
                    return;
                }

                try
                {
                    //Current employee
                    Employee currentEmp = context.Employees.SingleOrDefault(x => x.emp_uid == empId);

                    //Update Account
                    var ac = context.Accounts.FirstOrDefault(x => x.ac_uid == currentEmp.ac_uid);
                    ac.username = username;
                    ac.password = passWord;
                    ac.role = position;
                    ac.modified = modifieddate;
                    context.SaveChanges();
                    //Update Employee
                    var emp = context.Employees.FirstOrDefault(x => x.emp_uid == currentEmp.emp_uid);
                    emp.firstname = firstname;
                    emp.lastname = lastname;
                    emp.address = address;
                    emp.phone = phone;
                    emp.birthdate = birth;
                    emp.gender = (short)gender;
                    emp.img = strImg;
                    emp.modified = modifieddate;
                    emp.status = status;

                    context.SaveChanges();
                }
                catch(Exception ex)
                {
                    this.Close();
                }
                this.Close();
            }
        }

        private void btnCacel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void imgViewer_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "c:\\";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            if (dialog.ShowDialog() == true)
            {
                string selectedFileName = dialog.FileName;
                //Copy to new folder
                string filename = Path.GetFileName(selectedFileName);
                var environment = Environment.CurrentDirectory;
                string dicPath = Directory.GetParent(environment).Parent.FullName + "\\img\\employee\\" + filename;
                if (!File.Exists(dicPath))
                {
                    File.Copy(selectedFileName, dicPath);
                }
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Directory.GetParent(environment).Parent.FullName + "\\img\\employee\\" + filename);
                bitmap.EndInit();
                imgViewer.Source = bitmap;
                strImg = "\\img\\employee\\" + filename;
            }
        }
    }
}
