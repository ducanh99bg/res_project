﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using RES.Common;
using RES.Models;

namespace RES.AdminWorkPlace
{
    /// <summary>
    /// Interaction logic for DepartmentPage.xaml
    /// </summary>
    ///
    
    public partial class DepartmentPage : Page
    {
        internal List<Department> deList;
        public DepartmentPage()
        {
            InitializeComponent();
            deList = new List<Department>();
        }

        private void SetItemSource(string strSearch, int status) {
            try
            {
                using(var context = new RESContext())
                {
                    deList = context.Departments.ToList();
                    if(status != 0)
                    {
                        deList = deList.Where(x => x.status == status).ToList();
                    }
                    if (!string.IsNullOrEmpty(strSearch))
                    {
                        deList = deList.Where(x => x.name.Contains(strSearch)).ToList();
                    }
                    lvDepartment.ItemsSource = deList;
                };
            }
            catch(Exception ex)
            {

            }
        }

        private void btnDepartmentNew_Click(object sender, RoutedEventArgs e)
        {
            NewDepartmentWindow newDePage = new NewDepartmentWindow();
            newDePage.ShowDialog();
            SetItemSource(txtSearch.Text+"",cbxStatus.SelectedIndex);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //Set source for cbxStatus
            cbxStatus.ItemsSource = mdlMain.dicStatus;
            SetItemSource(txtSearch.Text + "", cbxStatus.SelectedIndex);
        }

        private void btnDepartmentDelete_Click(object sender, RoutedEventArgs e)
        {
            using(var context = new RESContext())
            {
                var selectedItem = lvDepartment.SelectedItems;
                for (int i = 0; i < selectedItem.Count; i++)
                {
                    Guid guid = Guid.Parse(selectedItem[i].GetType().GetProperties().First(x => x.Name == "de_uid").GetValue(selectedItem[i], null) + "");
                    var currentDepartment = context.Departments.FirstOrDefault(x => x.de_uid == guid);
                    currentDepartment.status = 1;
                    context.SaveChanges();
                }
            }
            SetItemSource(txtSearch.Text + "", cbxStatus.SelectedIndex);
        }

        private void cbxStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource(txtSearch.Text + "", cbxStatus.SelectedIndex);
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource(txtSearch.Text + "", cbxStatus.SelectedIndex);
        }

        private void lvDepartment_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ListView items = sender as ListView;
            Department de = (Department)items.SelectedItem;
            if(de != null)
            {
                DetailDepartmentWindow detail = new DetailDepartmentWindow();
                detail.deId = de.de_uid;
                detail.ShowDialog();
            }
            SetItemSource(txtSearch.Text + "", cbxStatus.SelectedIndex);
        }

        private void cbxAll_Checked(object sender, RoutedEventArgs e)
        {
            lvDepartment.SelectAll();
        }

        private void cbxAll_Unchecked(object sender, RoutedEventArgs e)
        {
            lvDepartment.UnselectAll();
        }
    }
}
