﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("Product", Schema = "public")]
    public class Product
    {
        [Key]
        [Column("product_uid", Order = 0)]
        public Guid product_uid { get; set; }
        [Required][MaxLength(100)]
        [Index(IsUnique = true)]
        public string name { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public double price { get; set; }
        [Required]
        public string img { get; set; }
        [Required]
        public DateTime created { get; set; }
        [Required]
        public DateTime modified { get; set; }
        [Required]
        public Int16 forsale { get; set; }
        [Required]
        public Int16 status { get; set; }
        [Required]
        public Guid ca_uid { get; set; }
        [ForeignKey("ca_uid")]
        public Category Category { get; set; }
    }
}
