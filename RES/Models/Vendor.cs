﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("Vendor", Schema="public")]
    class Vendor
    {
        [Key]
        [Column("ven_uid", Order=0)]
        public Guid ven_uid { get; set; }
        [Required, MaxLength(100)]
        [Index(IsUnique = true)]
        public string name { get; set; }
        [Required]
        public string address { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string phone { get; set; }

        [Required]
        public int status { get; set; }
    }
}
