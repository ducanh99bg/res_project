﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("StoreTransfer", Schema="public")]

    class StoreTransfer
    {
        [Key]
        [Column("sto_uid", Order =  0)]
        public Guid sto_uid { get; set; }
        [Required]
        public DateTime date { get; set; }
        [Required]
        public int status { get; set; }
        [Required]
        public Guid ven_uid { get; set; }
        [ForeignKey("ven_uid")]
        public Vendor Vendor { get; set; }
    }
}
