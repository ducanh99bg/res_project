﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("PromotionDetail", Schema = "public")]
    public class PromotionDetail
    {
        [Key]
        [Column("prodetail_uid", Order = 0)]
        public Guid prodetail_uid { get; set; }
        [Required]
        public Guid pro_uid { get; set; }
        [ForeignKey("pro_uid")]
        public Promotion Promotion { get; set; }
        public int type { get; set; }
        public Guid product_id { get; set; }
        public double discount_product { get; set; }
        public double start_value { get; set; }
        public double discount_totalbill { get; set; }
    }
}
