﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("InventoryAdjustment", Schema="public")]
    class InventoryAdjustment
    {
        [Key]
        [Column("inad_uid", Order = 0)]
        public Guid inad_uid { get; set; }
        [Required]
        public Guid inv_uid { get; set; }
        [ForeignKey("inv_uid")]
        public Inventory Inventory { get; set; }
        [Required]
        public double quatity { get; set; }
        [Required, MaxLength(200)]
        public string reason { get; set; }
        [Required]
        public DateTime date { get; set; }

    }
}
