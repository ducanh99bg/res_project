﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("StoreTransferDetail", Schema="public")]
    class StoreTransferDetail
    {
        [Key]
        [Column("stotransfer_uid", Order = 0)]
        public Guid stotransfer_uid { get; set; }
        [Required]
        public Guid sto_uid { get; set; }
        [ForeignKey("sto_uid")]
        public StoreTransfer StoreTransfer { get; set; }
        [Required]
        public Guid inv_uid { get; set; }
        [ForeignKey("inv_uid")]
        public Inventory Inventory { get; set; }
        [Required]
        public int quatity { get; set; }
        [Required]
        public double cost { get; set; }
        [Required]
        public int stockunit { get; set; }
    }
}
