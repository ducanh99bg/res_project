﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("Register", Schema="public")]
    class Register
    {
        [Key]
        [Column("re_uid", Order = 0)]
        public Guid re_uid { get; set; }
        [Required, MaxLength(100)]
        public string name { get; set; }
        [Required]
        public double openingamount { get; set; }
        [Required]
        public double cashsale { get; set; }
        [Required]
        public double refunds { get; set; }
        [Required]
        public double closeamount { get; set; }
        [Required]
        public DateTime start { get; set; }
        [Required]
        public DateTime end { get; set; }
        [Required]
        public Guid emp_uid { get; set; }
        [ForeignKey("emp_uid")]
        public Employee Employee { get; set; }
    }
}
