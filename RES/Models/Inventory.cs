﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("Inventory", Schema ="public")]
    class Inventory
    {
        [Key]
        [Column("inv_uid", Order = 0)]
        public Guid inv_uid { get; set; }
        [Required, MaxLength(100)]
        [Index(IsUnique = true)]
        public string name { get; set; }
        [Required]
        public string img { get; set; }
        [Required]
        public int type { get; set; }
        [Required]
        public double cost { get; set; }
        [Required]
        public double quatity { get; set; }
        [Required]
        public int stockunit { get; set; }
        [Required]
        public int status { get; set; }
        [Required]
        public Guid ven_uid { get; set; }
        [ForeignKey("ven_uid")]
        public Vendor Vendor { get; set; }
    }
}
