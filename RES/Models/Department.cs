﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("Department", Schema="public")]
    public class Department
    {
        [Key]
        [Column("de_uid", Order = 0)]
        public Guid de_uid { get; set; }
        [Index(IsUnique = true)]
        [MaxLength(100), Required]
        public string name { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public Int16 status { get; set; }
    }
}
