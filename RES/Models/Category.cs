﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("Category", Schema="public")]
    public class Category
    {
        [Key]
        [Column("ca_uid", Order = 0)]
        public Guid ca_uid { get; set; }
        [MaxLength(100)]
        [Required]
        [Index(IsUnique = true)]
        public string name { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public Int16 status { get; set; }
        [Required]
        public Guid de_uid { get; set; }
        [ForeignKey("de_uid")]
        public Department Department { get; set; }
    }
}
