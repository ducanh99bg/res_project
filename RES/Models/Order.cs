﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("Order", Schema = "public")]
    public class Order
    {
        [Key]
        [Column("or_uid", Order = 0)]
        public Guid or_uid { get; set; }
        [Required, MaxLength(100)]
        public string name { get; set; }
        [Required]
        public int type { get; set; }
        [Required]
        public int discount { get; set; }
        [Required]
        public double total { get; set; }
        [Required]
        public double pay { get; set; }
        [Required]
        public double repay { get; set; }
        [Required]
        public DateTime created { get; set; }
        [Required]
        public Int16 status { get; set; }
        [Column("emp_uid", Order = 2)]
        public Guid emp_uid { get; set; }
        [ForeignKey("emp_uid")]
        public Employee Employee { get; set; }
        public Guid cus_uid { get; set; }
        public Guid re_uid { get; set; }
    }
}
