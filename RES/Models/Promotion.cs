﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("Promotion", Schema="public")]
    public class Promotion
    {
        [Key]
        [Column("pro_uid", Order = 0)]
        public Guid pro_uid { get; set; }
        [Required, MaxLength(100)]
        public string title { get; set; }
        [Required, MaxLength(500)]
        public string description { get; set; }
        [Required]
        public string timestart { get; set; }
        [Required]
        public string timeend { get; set; }
        [Required]
        public DateTime start { get; set; }
        [Required]
        public DateTime end { get; set; }
        [Required]
        public int status { get; set; }
    }
}
