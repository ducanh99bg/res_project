﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RES.Models
{
    [Table("OrderDetail", Schema = "public")]
    public class OrderDetail
    {
        [Key]
        [Column("ordetail_uid", Order = 0)]
        public Guid ordetail_uid { get; set; }
        public Guid or_uid { get; set; }
        [ForeignKey("or_uid")]
        public Order Order { get; set; }
        [Required]
        public Guid po_uid { get; set; }
        [ForeignKey("po_uid")]
        public Product Product { get; set; }
        [Required]
        public int quatity { get; set; }
        [Required]
        public double discount { get; set; }
        [Required]
        public double price { get; set; }
        [Required]
        public string note { get; set; }
    }
}
