﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace RES.WareHouse
{
    /// <summary>
    /// Interaction logic for TransferExportWindow.xaml
    /// </summary>
    public partial class TransferExportWindow : Window
    {
        internal List<StoreTransferDetail> listTransfer = new List<StoreTransferDetail>();
        public TransferExportWindow()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    DateTime start = dptStart.SelectedDate.Value.Date.AddHours(0).AddMinutes(0);
                    DateTime end = dptEnd.SelectedDate.Value.Date.AddHours(23).AddMinutes(59);
                    int vendor = cbxVendors.SelectedIndex;
                    int type = cbxType.SelectedIndex;
                    List<StoreTransfer> lst = context.StoreTransfers.Where(y => y.date >= start && y.date <= end).ToList();
                    if (vendor != 0)
                    {
                        string name = mdlMain.getVendor()[vendor];
                        Guid venCode = context.Vendors.FirstOrDefault(x => x.name == name).ven_uid;
                        lst = lst.Where(x => x.ven_uid == venCode).ToList();
                    }
                    foreach (StoreTransfer st in lst)
                    {
                        List<StoreTransferDetail> tmp = context.StoreTransferDetails.Where(x => x.sto_uid == st.sto_uid).ToList();
                        if(type != 0)
                        {
                            tmp = tmp.Where(x => type == context.Inventorys.FirstOrDefault(y => y.inv_uid == x.inv_uid).type).ToList();
                        }
                        listTransfer.AddRange(tmp);
                    }
                    PdfFile();
                    this.Close();
                }

            }
            catch (Exception ex)
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dptStart.SelectedDate = DateTime.Now.Date;
            dptEnd.SelectedDate = DateTime.Now.Date;
            cbxVendors.ItemsSource = mdlMain.getVendor();
            cbxType.ItemsSource = mdlMain.dicInventoryType;
        }
        public void PdfFile()
        {
            try
            {
                string fontPath = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "\\Font\\ARIALUNI.TTF";
                Font f = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                f.Size = 10;
                f.SetStyle(Font.NORMAL);

                Font title = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                title.Size = 20;
                title.SetStyle(Font.NORMAL);

                Document pdfDoc = new Document(PageSize.A4.Rotate(), 20f, 20f, 40f, 40f);
                string path = $"E:\\bill\\warehouse2.pdf";
                PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.Create));
                pdfDoc.Open();
                pdfDoc.AddTitle("Thống kê bán hàng");
                pdfDoc.Add(new Paragraph("Thống kê nhập hàng", title)
                {
                    SpacingBefore = 40f,
                    SpacingAfter = 40f,
                    Alignment = Element.ALIGN_CENTER
                });
                var headerTable = new PdfPTable(new[] { 2f, 2f, 3f, 2f })
                {
                    WidthPercentage = 75,
                    DefaultCell = { MinimumHeight = 22f },

                };

                var startDate = new Paragraph("Từ ngày: " + dptStart.SelectedDate.Value.ToString("dd-MM-yyyy"), f);
                var endDate = new Paragraph("Tới ngày: " + dptEnd.SelectedDate.Value.ToString("dd-MM-yyyy"), f);
                var typeInv = new Paragraph("Loại mặt hàng: " + mdlMain.dicInventoryType[cbxType.SelectedIndex].ToString(), f);
                var vendorInv = new Paragraph("Nhà cung cấp: " + mdlMain.getVendor()[cbxVendors.SelectedIndex].ToString(), f);

                headerTable.DefaultCell.Border = Rectangle.NO_BORDER;
                headerTable.AddCell(startDate);
                headerTable.AddCell(endDate);
                headerTable.AddCell(typeInv);
                headerTable.AddCell(vendorInv);
                pdfDoc.Add(headerTable);

                //Table 1
                var columnWidths = new[] { 1f, 1f, 1f, 1f, 1f, 1f, 1f };
                var table = new PdfPTable(columnWidths)
                {
                    WidthPercentage = 100,
                    DefaultCell = { MinimumHeight = 22f }

                };

                var inven = new Paragraph("Mặt hàng", f);
                var vendor = new Paragraph("Nhà cung cấp", f);
                var type = new Paragraph("Loại", f);
                var time = new Paragraph("Thời gian", f);
                var quatity = new Paragraph("Số lượng nhập", f);
                var stockunit = new Paragraph("Đơn vị", f);
                var cost = new Paragraph("Giá nhập", f);

                table.DefaultCell.Border = Rectangle.BOTTOM_BORDER;

                table.AddCell(inven);
                table.AddCell(vendor);
                table.AddCell(type);
                table.AddCell(time);
                table.AddCell(quatity);
                table.AddCell(stockunit);
                table.AddCell(cost);

                using (var context = new RESContext())
                {
                    foreach (StoreTransferDetail item in listTransfer)
                    {
                        Inventory inv = context.Inventorys.FirstOrDefault(x => x.inv_uid == item.inv_uid);

                        var invenItem = new Paragraph(inv.name, f);

                        string ven_name = context.Vendors.FirstOrDefault(x => x.ven_uid == inv.ven_uid).name;
                        var vendorItem = new Paragraph(ven_name, f);

                        string type_name = mdlMain.dicInventoryType[inv.type].ToString();
                        var typeItem = new Paragraph(type_name, f);

                        string stock_name = mdlMain.dicStockUnit[item.stockunit];
                        var stockItem = new Paragraph(stock_name, f);

                        var timeItem = new Paragraph(context.StoreTransfers.FirstOrDefault(x => x.sto_uid == item.sto_uid).date.ToString("dd-MM-yyyy"), f);

                        var quatityItem = new Paragraph(item.quatity.ToString(), f);
                        var costItem = new Paragraph(item.cost.ToString("###,###,###") + " VND", f);


                        table.AddCell(invenItem);
                        table.AddCell(vendorItem);
                        table.AddCell(typeItem);
                        table.AddCell(timeItem);
                        table.AddCell(quatityItem);
                        table.AddCell(stockItem);
                        table.AddCell(costItem);
                    }
                }
                pdfDoc.Add(table);
                pdfDoc.Close();
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
