﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using RES.Common;
using RES.Models;

namespace RES.WareHouse
{
    /// <summary>
    /// Interaction logic for TransferPage.xaml
    /// </summary>
    public partial class TransferPage : Page
    {
        public class InventoryTransfer
        {
            public Guid inv_uid { get; set; }
            public string inv_name { get; set; }
            public Guid ven_uid { get; set; }
            public string vendor_name { get; set; }
            public string stockunit { get; set; }
            public int quatity { get; set; }
            public double cost { get; set; }

        }
        List<InventoryTransfer> listInvTransfer;
        public TransferPage()
        {
            InitializeComponent();
        }
        public void SetItemSource()
        {
            try
            {
                listInvTransfer = new List<InventoryTransfer>();
                string search = txtSearch.Text + "";
                int vendor = cbxVendor.SelectedIndex;
                using (var context = new RESContext())
                {
                    foreach (Inventory inv in context.Inventorys.Where(x => x.status == 2).ToList())
                    {
                        InventoryTransfer i = new InventoryTransfer()
                        {
                            inv_uid = inv.inv_uid,
                            inv_name = inv.name,
                            ven_uid = context.Vendors.FirstOrDefault(x => x.ven_uid == inv.ven_uid).ven_uid,
                            vendor_name = context.Vendors.FirstOrDefault(x => x.ven_uid == inv.ven_uid).name + "",
                            stockunit = mdlMain.dicStockUnit[inv.stockunit],
                            quatity = 0,
                            cost = 0
                        };
                        listInvTransfer.Add(i);
                    }
                    if (vendor != 0)
                    {
                        int num = cbxVendor.SelectedIndex;
                        string name = mdlMain.getVendor()[num];
                        Guid venCode = context.Vendors.FirstOrDefault(x => x.name == name).ven_uid;
                        listInvTransfer = listInvTransfer.Where(x => x.ven_uid == venCode).ToList();
                    }
                    if (!string.IsNullOrEmpty(search))
                    {
                        listInvTransfer = listInvTransfer.Where(x => x.inv_name.Contains(search)).ToList();
                    }
                    lvTransfer.ItemsSource = listInvTransfer.ToList();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            cbxVendor.ItemsSource = mdlMain.getVendor().Where(x => x.Key != 0);
            SetItemSource();
        }

        private void cbxVendor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource();
        }

        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    int num = cbxVendor.SelectedIndex;
                    string name = mdlMain.getVendor()[num + 1];
                    Guid venCode = context.Vendors.FirstOrDefault(x => x.name == name).ven_uid;
                    StoreTransfer st = new StoreTransfer()
                    {
                        sto_uid = Guid.NewGuid(),
                        date = DateTime.Now,
                        status = 2,
                        ven_uid = venCode
                    };
                    context.StoreTransfers.Add(st);
                    foreach (InventoryTransfer item in lvTransfer.Items)
                    {
                        InventoryTransfer iv = listInvTransfer.FirstOrDefault(x => x.inv_uid == item.inv_uid);
                        iv.quatity = item.quatity;
                        iv.cost = item.cost;

                        StoreTransferDetail detail = new StoreTransferDetail()
                        {
                            stotransfer_uid = Guid.NewGuid(),
                            sto_uid = st.sto_uid,
                            inv_uid = iv.inv_uid,
                            quatity = iv.quatity,
                            cost = iv.cost,
                            stockunit = mdlMain.dicStockUnit.FirstOrDefault(x => x.Value == item.stockunit).Key
                        };
                        Inventory i = context.Inventorys.FirstOrDefault(x => x.inv_uid == detail.inv_uid);
                        if (detail.cost != i.cost)
                        {
                            i.cost = detail.cost;
                        }
                        context.StoreTransferDetails.Add(detail);
                    }
                    context.SaveChanges();
                    SetItemSource();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnExport_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            TransferExportWindow export = new TransferExportWindow();
            export.ShowDialog();
        }
    }
}
