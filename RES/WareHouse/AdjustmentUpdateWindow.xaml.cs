﻿using RES.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using RES.Models;

namespace RES.WareHouse
{
    /// <summary>
    /// Interaction logic for AdjustmentUpdateWindow.xaml
    /// </summary>
    public partial class AdjustmentUpdateWindow : Window
    {
        internal Guid inv_uid;
        internal Inventory inventory;
        public AdjustmentUpdateWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbxReason.ItemsSource = mdlMain.dicReasonAdjust;
            using (var context = new RESContext())
            {
                inventory = context.Inventorys.FirstOrDefault(x => x.inv_uid == inv_uid);
                txtQuatity.Text = inventory.quatity.ToString();
                txtname.Text = inventory.name.ToString();
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int quatity = 0;
                int.TryParse(txtQuatity.Text + "", out quatity);
                int use = 0;
                int.TryParse(txtUse.Text + "", out use);
                string reason = "";
                if (cbxReason.SelectedIndex != 0)
                {
                    reason = cbxReason.Text;
                }
                else reason = txtReason.Text + "";

                using(var context = new RESContext())
                {
                    if(quatity >= 0)
                    {
                        if (use > 0 && use <= quatity && !string.IsNullOrEmpty(reason))
                        {
                            InventoryAdjustment in_ad = new InventoryAdjustment()
                            {
                                inad_uid = Guid.NewGuid(),
                                inv_uid = inv_uid,
                                quatity = use,
                                reason = reason,
                                date = DateTime.Now
                            };
                            context.InventoryAdjustments.Add(in_ad);
                            inventory = context.Inventorys.FirstOrDefault(x => x.inv_uid == inv_uid);
                            inventory.quatity = quatity;
                            inventory.quatity -= use;
                        }
                        else
                        {
                            MessageBox.Show("Số lượng cập nhật vượt quá số lượng trong kho");
                        }
                    }
                    context.SaveChanges();
                }
                this.Close();
            }
            catch(Exception ex)
            {

            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void cbxReason_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if(cbxReason.SelectedIndex != 0)
            {
                txtReason.Visibility = Visibility.Hidden;
            }
            else
            {
                txtReason.Visibility = Visibility.Visible;
            }
        }

    }
}
