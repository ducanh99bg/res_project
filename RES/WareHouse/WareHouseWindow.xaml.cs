﻿using System;
using System.Windows;

namespace RES.WareHouse
{
    /// <summary>
    /// Interaction logic for WareHouseWindow.xaml
    /// </summary>
    public partial class WareHouseWindow : Window
    {
        public WareHouseWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WarehouseFrame.Navigate(new AdjustmentPage());
        }

        private void btnAdjustment_Click(object sender, RoutedEventArgs e)
        {
            WarehouseFrame.Navigate(new AdjustmentPage());
        }

        private void btnTransfer_Click(object sender, RoutedEventArgs e)
        {
            WarehouseFrame.Navigate(new TransferPage());
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
