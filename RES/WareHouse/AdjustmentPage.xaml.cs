﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;


namespace RES.WareHouse
{
    /// <summary>
    /// Interaction logic for AdjustmentPage.xaml
    /// </summary>
    public partial class AdjustmentPage : Page
    {
        internal List<Inventory> lst;
        public AdjustmentPage()
        {
            InitializeComponent();
        }

        public void SetItemSource()
        {
            try
            {
                lst = new List<Inventory>();
                string search = txtSearch.Text + "";
                int vendor = cbxVendor.SelectedIndex;
                using(var context = new RESContext()) {
                    lst = context.Inventorys.Where(x => x.status == 2).ToList();
                    if(vendor != 0)
                    {
                        int num = cbxVendor.SelectedIndex;
                        string name = mdlMain.getVendor()[num];
                        Guid venCode = context.Vendors.FirstOrDefault(x => x.name == name).ven_uid;
                        lst = lst.Where(x => x.ven_uid == venCode).ToList();
                    }
                    if (!string.IsNullOrEmpty(search))
                    {
                        lst = lst.Where(x => x.name.Contains(search)).ToList();
                    }
                    lvAdjustment.ItemsSource = lst;
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            cbxVendor.ItemsSource = mdlMain.getVendor();
            SetItemSource();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxVendor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void txtQuatity_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            string quatity = ((TextBox)sender).Text;
        }

        private void lvAdjustment_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                ListView items = sender as ListView;
                Inventory inv = (Inventory)items.SelectedItem;
                if (inv != null)
                {
                    AdjustmentUpdateWindow ad = new AdjustmentUpdateWindow();
                    ad.inv_uid = inv.inv_uid;
                    ad.ShowDialog();
                    SetItemSource();
                }
            }
            catch(Exception ex)
            {
            }
        }

        private void btnExport_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            AdjustExportWindow export = new AdjustExportWindow();
            export.ShowDialog();
        }

        private void cbxVendor_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }
    }
}
