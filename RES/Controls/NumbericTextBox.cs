﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;

namespace RES.Controls
{
    class NumbericTextBox : TextBox
    {
		public double NumericValue { get { return value; } }
		private double value;
		public NumbericTextBox() : base()
		{
			TextChanged += NumericTextBox_TextChanged;
			PreviewTextInput += NumericTextBox_PreviewTextInput;
			//TextAlignment = TextAlignment.Rig;
		}

		private void NumericTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			Regex regex = new Regex("[^0-9]+");
			e.Handled = regex.IsMatch(e.Text);
		}
		bool textOnChanging = false;
		private void NumericTextBox_TextChanged(object sender, EventArgs e)
		{
			try
			{
				if (!textOnChanging)
				{
					textOnChanging = true;
					double.TryParse(Text, out value);
					//Text = value.ToString("###,###,###");
					//this.SelectionStart = Text.Length;
					textOnChanging = false;
				}
			}
			catch (Exception ex)
			{
			}
		}
	}
}
