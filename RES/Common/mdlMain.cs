﻿using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RES.Common
{
    public static class mdlMain
    {

        public static Guid cus_id;
        public static Guid order_id;
        public static Guid emp_id;
        public static List<OrderDetail> orderDetails;
        public static Guid register_id;

        public static List<Product> menus = new List<Product>();

        enum FORSALE
        {
            ALL = 0,
            SALE = 1,
            STOP_SALE = 2
        }

        public static Dictionary<int, string> dicForSale = new Dictionary<int, string>()
        {
            {0,"Tất cả"},{1,"Đang bán"},{2,"Dừng bán"}
        };

        enum POSITION
        {
            ALL = 0,
            CHEF = 1,
            WAITER = 2,
            RECEPTIONIST = 3,
            WAREHOUSESTAFF = 4,
            MANAGER = 5
        }

        internal static OrderDetail FirstOrDefault(object x)
        {
            throw new NotImplementedException();
        }

        public static Dictionary<int, string> dicPosition = new Dictionary<int, string>() {
            { 0,"Tất cả"},{1,"Đầu bếp"},{2,"Bồi bàn"},{3,"Nhân viên bán hàng"},{4,"Quản lý kho hàng"},{5,"Quản lý nhà hàng"}
        };

        enum STATUS
        {
            ALL = 0,
            LOCK = 1,
            ACTIVE = 2
        }
        public static Dictionary<int, string> dicStatus = new Dictionary<int, string>()
        {
            {0,"Tất cả"}, {1,"Đã khóa"}, {2,"Đang hoạt động"}
        };
        enum SALETYPE
        {
            DISCOUNT_PRODUCT = 0,
            DISCOUNT_BILL = 1,
        }
        public static Dictionary<int, string> dicSale = new Dictionary<int, string>()
        {
            {0,"Giảm giá theo sản phẩm"},{1,"Giảm giá theo hóa đơn"}
        };

        public static Dictionary<int, string> dicCate = new Dictionary<int, string>();
        public static Dictionary<int, string> getCategory()
        {
            dicCate = new Dictionary<int, string>();
            List<Category> lstCategory = new List<Category>();
            using (var context = new RESContext())
            {
                lstCategory = context.Categories.Where(x => x.status == 2).ToList();
            };
            dicCate.Add(0, "Tất cả");
            int i = 1;
            foreach (Category item in lstCategory)
            {
                dicCate.Add(i, item.name);
                i++;
            }
            return dicCate;
        }
        public static Dictionary<int, string> dicCateByDe = new Dictionary<int, string>();

        public static Dictionary<int, string> getCategoryByDepartment(Guid decode)
        {
            dicCateByDe = new Dictionary<int, string>();
            List<Category> lstCate = new List<Category>();
            using (var context = new RESContext())
            {
                lstCate = context.Categories.Where(x => x.de_uid == decode && x.status == 2).ToList();
            }
            int i = 0;
            foreach (Category item in lstCate)
            {
                dicCateByDe.Add(i, item.name);
                i++;
            }
            return dicCateByDe;
        }

        public static Dictionary<int, string> dicDe = new Dictionary<int, string>();

        public static Dictionary<int, string> getDepartment()
        {
            dicDe = new Dictionary<int, string>();
            List<Department> lstDepartment = new List<Department>();
            using (var context = new RESContext())
            {
                lstDepartment = context.Departments.Where(x => x.status == 2).ToList();
            };
            dicDe.Add(0, "Tất cả");
            int i = 1;
            foreach (Department item in lstDepartment)
            {
                dicDe.Add(i, item.name);
                i++;
            }
            return dicDe;
        }


        public static Dictionary<int, string> dicProduct = new Dictionary<int, string>();
        public static Dictionary<int, string> getProductBuCategory(Guid id)
        {
            dicProduct = new Dictionary<int, string>();
            List<Product> lst = new List<Product>();
            using (var context = new RESContext())
            {
                lst = context.Products.Where(x => x.ca_uid == id & x.status == 2).ToList();
            }
            dicProduct.Add(0, "Tất cả");
            int i = 1;
            foreach (Product item in lst)
            {
                dicProduct.Add(i, item.name);
                i++;
            }
            return dicProduct;
        }

        public static List<Promotion> getPromotions()
        {
            List<Promotion> listPromotion = new List<Promotion>();

            using(var context = new RESContext())
            {
                listPromotion = context.Promotions.Where(x => x.status == 2).ToList();
                listPromotion = listPromotion.Where(x => DateTime.Compare(x.start, DateTime.Now) <= 0 && DateTime.Compare(x.end, DateTime.Now) >= 0).ToList();
                int timeNow = Convert.ToInt32(DateTime.Now.Hour.ToString()+DateTime.Now.Minute.ToString());
                listPromotion = listPromotion.Where(x => Convert.ToInt32(x.timestart) <= timeNow && Convert.ToInt32(x.timeend) >= timeNow).ToList();
            }
            return listPromotion;
        }

        enum QuatityStatus{
            ALL=0,
            Stocking = 1,
            OutOfStocking = 2
        }

        public static Dictionary<int, string> dicQuatityStatus = new Dictionary<int, string>()
        {
            {0,"Tất cả"},{1,"Còn trong kho"},{2,"Hết hàng"} 
        };

        public static Dictionary<int, string> dicVendor = new Dictionary<int, string>();
        public static Dictionary<int, string> getVendor()
        {
            dicVendor = new Dictionary<int, string>();
            List<Vendor> lst = new List<Vendor>();
            using(var context = new RESContext())
            {
                lst = context.Vendors.Where(x => x.status == 2).ToList();
            }
            dicVendor.Add(0, "Tất cả");
            int i = 1;
            foreach (Vendor item in lst)
            {
                dicVendor.Add(i,item.name);
                i++;
            }
            return dicVendor;
        }

        enum InventoryType
        {
            ALL = 0,
            FRESH_FOOD = 1,
            CANNED_FOOD = 2,
            BOTTLED = 3,
            SPICE = 4
        };
        public static Dictionary<int, string> dicInventoryType = new Dictionary<int, string> {
            {0,"Tất cả"},{1,"Thực phẩm tươi sống"},{2,"Hàng đóng hộp"},{3,"Đóng chai"},{4,"Gia vị"}
        };

        enum StockUnit
        {
            ALL = 0,
            PACK = 1,
            BOX = 2,
            BOTTLE = 3
        }
        public static Dictionary<int, string> dicStockUnit = new Dictionary<int, string>()
        {
            {0,"Tất cả"},{1,"Gói"},{2,"Hộp"},{3,"Chai"}
        };

        enum TypeSale
        {
            ALL = 0,
            QUICK_SALE = 1,
            DINE_IN = 2,
        }

        public static Dictionary<int, string> dicTypeSale = new Dictionary<int, string>()
        {
            {0,"Tất cả"},{1,"Mua hàng nhanh"},{2,"Ăn tại chỗ"}
        };

        enum StatusOrder
        {
            ALL = 0,
            PAID = 1,
            UNPAID = 2
        }

        public static Dictionary<int, string> dicStatusOrder = new Dictionary<int, string>()
        {
            {0,"Tất cả"},{1,"Đã thanh toán"},{2,"Chưa thanh toán"}
        };
        public static Dictionary<int, string> dicReportProduct = new Dictionary<int, string>()
        {
            {0,"Số lượng"},{1,"Doanh thu"}
        };
        public static Dictionary<int, string> dicReasonAdjust = new Dictionary<int, string>()
        {
            {0,"Khác"},{1,"Đã sử dụng"},{2,"Hỏng/Đổ vỡ"}, {3,"Hết hạn sử dụng"}
        };
        public static Dictionary<int, string> dicCustomerStatus = new Dictionary<int, string>()
        {
            {0,"Tất cả"},{1,"Chưa thanh toán"},{2,"Đã thanh toán hết"}
        };
    }
}
