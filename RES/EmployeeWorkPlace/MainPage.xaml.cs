﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        public class CustomerSearch
        {
            public Guid cus_uid { get; set; }
            public string searchinfor { get; set; }
        }
        public List<CustomerSearch> comboboxConvert(List<Customer> cuss)
        {
            List<CustomerSearch> lst = new List<CustomerSearch>();
            foreach (Customer cus in cuss)
            {
                CustomerSearch cs = new CustomerSearch
                {
                    cus_uid = cus.cus_uid,
                    searchinfor = cus.firstname + " " + cus.lastname + "-"+cus.phone
                };
                lst.Add(cs);
            }
            return lst;
        }
        internal List<Customer> customers;
        public MainPage()
        {
            InitializeComponent();
        }

        public void SetOrder()
        {
            if(mdlMain.orderDetails.Count > 0)
            {
                lvOrdering.ItemsSource = mdlMain.orderDetails.ToList();
                double total = 0;
                foreach(OrderDetail item in mdlMain.orderDetails)
                {
                    total += item.quatity * item.price * (100 - item.discount) / 100;
                }
                using(var context = new RESContext())
                {
                    List<PromotionDetail> details = context.PromotionDetails.Where(x => x.type == 2 && context.Promotions.FirstOrDefault(y => y.pro_uid == x.pro_uid).status == 2).OrderBy(x => x.start_value).ToList();
                    double discount = 0;
                    foreach(PromotionDetail item in details)
                    {
                        if(item.start_value <= total)
                        {
                            discount = item.discount_totalbill;
                        }
                    }
                    txtDiscount.Text = discount + "";
                    total = total * (1 - discount / 100);
                }
                txtTotal.Text = total.ToString("###,###,##0");
            }
            else
            {
                lvOrdering.ItemsSource = mdlMain.orderDetails.ToList();
                txtDiscount.Text = "0";
                txtTotal.Text = "0";
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void TextBlock_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            SearchCustomerWindow search = new SearchCustomerWindow();
            search.ShowDialog();
        }

        private void cbxCustomer_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            string search = cbxCustomer.Text + "";
            if (!string.IsNullOrEmpty(search))
            {
                using (var context = new RESContext())
                {
                    cbxCustomer.SelectedIndex = -1;
                    
                    cbxCustomer.ItemsSource = comboboxConvert(customers.Where(x => x.firstname.Contains(search) | x.lastname.Contains(search)).ToList());
                }
            }
            else mdlMain.cus_id = new Guid();
        }

        private void cbxCustomer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(cbxCustomer.SelectedIndex != -1 && cbxCustomer.SelectedItem != null && Guid.Parse(cbxCustomer.SelectedValue+"") != mdlMain.cus_id)
            {
                mdlMain.cus_id = Guid.Parse(cbxCustomer.SelectedValue + "");

                DetailCustomerPage detail = new DetailCustomerPage();
                detail.cus_id = mdlMain.cus_id;
                this.NavigationService.Navigate(detail);
            }
        }

        private void btnAddCustomer_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new AddNewCustomerPage());
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (mdlMain.order_id == new Guid() | mdlMain.order_id == null)
            {
                mdlMain.order_id = Guid.NewGuid();
                mdlMain.orderDetails = new List<OrderDetail>();
            }
            else
            {
                SetOrder();
            }

            using (var context = new RESContext())
            {
                customers = context.Customers.Where(x => x.status == 2).ToList();
                //Department
                cbxDepartment.ItemsSource = mdlMain.getDepartment().Where(x => x.Key != 0);
                //Category
                lbxProduct.ItemsSource = context.Products.Where(x => x.status == 2).ToList();
            }

            if (mdlMain.cus_id != null)
            {
                cbxCustomer.ItemsSource = comboboxConvert(customers.Where(x => x.cus_uid == mdlMain.cus_id).ToList());
                cbxCustomer.SelectedIndex = 0;

                cbxTypeSale.ItemsSource = mdlMain.dicTypeSale.Where(x => x.Key != 0);
                cbxCustomer.SelectedIndex = 0;
            }
        }

        private void cbxDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string dename = mdlMain.getDepartment()[cbxDepartment.SelectedIndex + 1];
            using(var context = new RESContext())
            {
                Guid decode = context.Departments.FirstOrDefault(x => x.name == dename).de_uid;
                lbxCategory.ItemsSource = context.Categories.Where(x => x.de_uid == decode && x.status == 2).ToList();

                lbxProduct.ItemsSource = context.Products.Where(x => context.Categories.FirstOrDefault(y => y.ca_uid == x.ca_uid).de_uid == decode && x.status == 2).ToList();
            }
        }

        private void lbxCategory_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListBox lbSelected = sender as ListBox;
            Category category = (Category)lbSelected.SelectedItem;
            if (category != null)
            {
                using(var context = new RESContext()) {
                    lbxProduct.ItemsSource = context.Products.Where(x => x.ca_uid == category.ca_uid && x.status == 2).ToList();
                }
            }
        }

        private void lbxProduct_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListBox lbSelected = sender as ListBox;
            Product product = (Product)lbSelected.SelectedItem;
            if (product != null)
            {
                double discount = 0;
                List<PromotionDetail> promotionProduct = new List<PromotionDetail>();
                List<Promotion> promotions = mdlMain.getPromotions();
                using (var context = new RESContext())
                {
                    foreach (Promotion i in promotions)
                    {
                        promotionProduct = context.PromotionDetails.Where(x => x.pro_uid == i.pro_uid).ToList();
                        promotionProduct = promotionProduct.Where(x => x.product_id == product.product_uid).ToList();
                        if (promotionProduct.Count > 0)
                        {
                            break;
                        }
                    }
                }
                foreach (PromotionDetail i in promotionProduct)
                {
                    discount += i.discount_product;
                }

                using (var context = new RESContext())
                {
                    //PromotionDetail poDetail = context.
                    OrderDetail orDetail = new OrderDetail()
                    {
                        ordetail_uid = Guid.NewGuid(),
                        or_uid = mdlMain.order_id,
                        po_uid = product.product_uid,
                        quatity = 1,
                        discount = discount,
                        price = product.price * (1 - discount * 0.01),
                        note = "abc"
                    };
                    if(mdlMain.orderDetails.FirstOrDefault(x => x.po_uid == orDetail.po_uid) != null)
                    {
                        mdlMain.orderDetails.FirstOrDefault(x => x.po_uid == orDetail.po_uid).quatity ++;
                    }
                    else
                    {
                        mdlMain.orderDetails.Add(orDetail);
                    }
                }
            }
            SetOrder();

        }

        private void btnRegister_Click(object sender, RoutedEventArgs e)
        {
            if(mdlMain.register_id == null | mdlMain.register_id == new Guid())
            {
                RegisterPage register = new RegisterPage();
                this.NavigationService.Navigate(register);
            }
            else
            {
                CloseRegisterPage close = new CloseRegisterPage();
                this.NavigationService.Navigate(close);
            }
        }

        private void btnSaveOrder_Click(object sender, RoutedEventArgs e)
        {
            if (mdlMain.register_id == null | mdlMain.register_id == new Guid())
            {
                MessageBox.Show("Bạn cần đăng ký ca làm việc !");
                return;
            }
            else
            {
                SaveOrderPage saveorder = new SaveOrderPage();
                saveorder.amount = Convert.ToDouble(txtTotal.Text + "");
                saveorder.discount = Convert.ToInt32(txtDiscount.Text + "");
                saveorder.type = cbxTypeSale.SelectedIndex + 1;
                this.NavigationService.Navigate(saveorder);
            }
        }
        private void lvOrdering_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ListView items = sender as ListView;
                OrderDetail order = (OrderDetail)items.SelectedItem;
                if (order != null)
                {
                    EditOrderQuatityWindow edit = new EditOrderQuatityWindow();
                    edit.or_id = order.ordetail_uid;
                    edit.ShowDialog();
                }
                SetOrder();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnOrder_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new HistoryOrderPage());
        }

        private void btnDeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            mdlMain.orderDetails = new List<OrderDetail>();
        }

        private void btnCash_Click(object sender, RoutedEventArgs e)
        {
            if (mdlMain.register_id == null | mdlMain.register_id == new Guid())
            {
                MessageBox.Show("Bạn cần đăng ký ca làm việc !");
                return;
            }
            if(mdlMain.orderDetails.Count == 0 | mdlMain.orderDetails == null){
                MessageBox.Show("Hóa đơn chưa có sản phẩm để tiến hành thanh toán !");
                return;
            }
            else
            {
                CashPage cash = new CashPage();
                using(var context = new RESContext())
                {
                    Order order = context.Orders.FirstOrDefault(x => x.or_uid == mdlMain.order_id);
                    double total = 0;
                    int discount = 0;
                    int.TryParse(txtDiscount.Text + "", out discount);
                    foreach (OrderDetail item in mdlMain.orderDetails)
                    {
                        total += item.quatity * item.price * (100 - item.discount) / 100;
                    }
                    total = total - total*discount/100;
                    if (order == null)
                    {
                        order = new Order()
                        {
                            or_uid = mdlMain.order_id,
                            name = "quick sale",
                            type = 1,
                            discount = discount,
                            total = total,
                            pay = 0,
                            repay = 0,
                            created = DateTime.Now,
                            status = 2,
                            emp_uid = mdlMain.emp_id,
                            cus_uid = mdlMain.cus_id,
                            re_uid = mdlMain.register_id
                        };
                        cash.order = order;
                        cash.quickSale = true;
                    }
                    else
                    {
                        order.discount = discount;
                        order.total = total;
                        context.SaveChanges();
                        cash.order = order;
                        cash.quickSale = false;
                    }
                }
                this.NavigationService.Navigate(cash);
            }
        }

        private void btnDeleteOrder_Click_1(object sender, RoutedEventArgs e)
        {
            mdlMain.order_id = new Guid();
            mdlMain.orderDetails = new List<OrderDetail>();
            SetOrder();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            using (var context = new RESContext())
            {
                string search = txtSearch.Text + "";
                string dename = mdlMain.getDepartment()[cbxDepartment.SelectedIndex + 1];
                Guid decode = context.Departments.FirstOrDefault(x => x.name == dename).de_uid;
                if (!string.IsNullOrEmpty(search))
                {
                    lbxProduct.ItemsSource = context.Products.Where(x =>x.name.Contains(search) && context.Categories.FirstOrDefault(y => y.ca_uid == x.ca_uid).de_uid == decode && x.status == 2).ToList();
                }
                else
                {
                    lbxProduct.ItemsSource = context.Products.Where(x => context.Categories.FirstOrDefault(y => y.ca_uid == x.ca_uid).de_uid == decode && x.status == 2).ToList();
                }
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if(mdlMain.register_id == null || mdlMain.register_id == new Guid())
            {
                Window parentWindow = Window.GetWindow(this);
                parentWindow.Close();
            }
            else
            {
                MessageBox.Show("Kết thúc ca làm việc để thoát !");
            }
        }
    }
}
