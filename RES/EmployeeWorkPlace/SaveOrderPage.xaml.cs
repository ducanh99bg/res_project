﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;


namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for SaveOrderPage.xaml
    /// </summary>
    public partial class SaveOrderPage : Page
    {
        internal double amount;
        internal int discount;
        internal int type;
        public SaveOrderPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            txtType.Text = mdlMain.dicTypeSale[type];
            lvOrdering.ItemsSource = mdlMain.orderDetails;
            txtDiscount.Text = discount + "";
            txtAmount.Text = amount.ToString();
            using(var context = new RESContext()) {
                txtEmployee.Text = context.Employees.FirstOrDefault(x => x.emp_uid == mdlMain.emp_id).lastname;
                txtOrderName.Text = context.Orders.FirstOrDefault(x => x.or_uid == mdlMain.order_id)?.name + "";
            }
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    // Kiểm tra hóa đơn tồn tại
                    Order order = context.Orders.FirstOrDefault(x => x.or_uid == mdlMain.order_id);
                    if(order != null)
                    {
                        // Xóa các bản ghi cũ để cập nhật detail mới
                        context.OrderDetails.RemoveRange(context.OrderDetails.Where(x => x.or_uid == order.or_uid).ToList());
                        order.discount = discount;
                        order.type = type;
                        order.total = amount;
                    }
                    else
                    {
                        //tạo hóa đơn mới
                        order = new Order()
                        {
                            or_uid = mdlMain.order_id,
                            name = txtOrderName.Text + "",
                            type = type,
                            discount = discount,
                            pay = 0,
                            repay = 0,
                            total = amount,
                            created = DateTime.Now,
                            status = 2,
                            emp_uid = mdlMain.emp_id,
                            cus_uid = mdlMain.cus_id,
                            re_uid = mdlMain.register_id
                        };
                        context.Orders.Add(order);
                    }
                    foreach (OrderDetail item in mdlMain.orderDetails)
                    {
                        context.OrderDetails.Add(item);
                    }
                    context.SaveChanges();
                    mdlMain.order_id = new Guid();
                    mdlMain.orderDetails = new List<OrderDetail>();
                }
            }
            catch(Exception ex)
            {

            }
            
            this.NavigationService.Navigate(new MainPage());
        }

        private void btnBack_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainPage());
        }
    }
}
