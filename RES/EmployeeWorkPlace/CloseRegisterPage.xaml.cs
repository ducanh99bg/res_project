﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for CloseRegisterPage.xaml
    /// </summary>
    public partial class CloseRegisterPage : Page
    {
        internal Register register;
        internal double sale;
        internal double pay;
        internal double repay;
        public CloseRegisterPage()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainPage());
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    register = context.Registers.FirstOrDefault(x => x.re_uid == mdlMain.register_id);
                    List<Order> orders = context.Orders.Where(x => x.re_uid == mdlMain.register_id && x.status == 1).ToList();
                    foreach(Order order in orders)
                    {
                        sale += order.total;
                        pay += order.pay;
                        repay += order.repay;
                    }
                    txtOpen.Text = register.start.ToString();
                    Employee emp = context.Employees.FirstOrDefault(x => x.emp_uid == mdlMain.emp_id);
                    txtEmployee.Text = emp.firstname + " " + emp.lastname;
                    txtOpenAmount.Text = register.openingamount + " VND";
                    txtCashSale.Text = sale + " VND";
                    txtPay.Text = pay + " VND";
                    //txtRepay.Text = repay + " VND";
                }
            }
            catch(Exception ex)
            {
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    register = context.Registers.FirstOrDefault(x => x.re_uid == mdlMain.register_id);
                    register.cashsale = sale;
                    double closeamount = 0;
                    double.TryParse(txtAmount.Text + "", out closeamount);
                    register.closeamount = closeamount;
                    register.end = DateTime.Now;
                    context.SaveChanges();
                }
                mdlMain.register_id = new Guid();
            }
            catch(Exception ex)
            {

            }
            this.NavigationService.Navigate(new MainPage());
        }
    }
}
