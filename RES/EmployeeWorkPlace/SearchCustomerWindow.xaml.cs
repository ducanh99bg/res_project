﻿using System;
using System.Windows;


namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for SearchCustomerWindow.xaml
    /// </summary>
    public partial class SearchCustomerWindow : Window
    {
        bool close = false;
        public SearchCustomerWindow()
        {
            InitializeComponent();
        }
        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            close = false;
            this.Close();

        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            if (close)
            {
                this.Close();
            }
        }
    }
}
