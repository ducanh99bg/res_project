﻿using RES.Models;
using System;
using System.Windows;
using System.Windows.Controls;

namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for AddNewCustomerPage.xaml
    /// </summary>
    public partial class AddNewCustomerPage : Page
    {
        public AddNewCustomerPage()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string firstname = txtFirstName.Text + "";
                string lastname = txtLastName.Text + "";
                string address = txtAddress.Text + "";
                string phone = txtPhone.Text + "";
                short gender = (rdMale.IsChecked == true) ? (short)1 : (short)2;
                DateTime birth = dptBirthDate.SelectedDate.Value;
                DateTime createdDate = DateTime.Now;

                using (var context = new RESContext())
                {
                    Customer cus = new Customer()
                    {
                        cus_uid = Guid.NewGuid(),
                        firstname = firstname,
                        lastname = lastname,
                        gender = gender,
                        birthdate = birth,
                        address = address,
                        phone = phone,
                        created = createdDate,
                        modified = createdDate,
                        status = 2
                    };
                    context.Customers.Add(cus);
                    context.SaveChanges();

                    DetailCustomerPage detail = new DetailCustomerPage();
                    detail.cus_id = cus.cus_uid;
                    this.NavigationService.Navigate(detail);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Thông tin khách hàng chưa chính xác !");
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainPage());
        }
    }
}
