﻿using RES.Common;
using RES.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for DetailOrderPage.xaml
    /// </summary>
    public partial class DetailOrderPage : Page
    {
        internal Guid order_uid;
        internal Order order;
        public DetailOrderPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try {
                using(var context = new RESContext())
                {
                    order = context.Orders.FirstOrDefault(x => x.or_uid == order_uid);

                    txtOrderUid.Text = order.or_uid + "";
                    txtname.Text = order.name;
                    txtDate.Text = order.created.Date + "";
                    txtType.Text = mdlMain.dicTypeSale[order.type] + "";
                    txtStatus.Text = mdlMain.dicStatusOrder[order.status] + "";
                    txtPay.Text = order.pay.ToString("###,###,###,##0") + " VND";
                    txtRepay.Text = order.repay.ToString("###,###,###,##0") + " VND";
                    txtAmount.Text = order.total.ToString("###,###,###,##0") + " VND";
                    txtDiscount.Text = order.discount + " %";

                    lvOrdering.ItemsSource = context.OrderDetails.Where(x => x.or_uid == order_uid).ToList();
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new HistoryOrderPage());
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            using(var context = new RESContext())
            {
                mdlMain.order_id = order_uid;
                mdlMain.orderDetails = context.OrderDetails.Where(x => x.or_uid == order_uid).ToList();
                mdlMain.cus_id = order.cus_uid;
                this.NavigationService.Navigate(new MainPage());
            }
        }
    }
}
