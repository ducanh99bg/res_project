﻿using RES.Common;
using RES.Models;
using System;
using System.Linq;
using System.Windows.Controls;


namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for DetailCustomerPage.xaml
    /// </summary>
    public partial class DetailCustomerPage : Page
    {
        internal Guid cus_id;
        public DetailCustomerPage()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainPage());
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    Customer cus = context.Customers.FirstOrDefault(x => x.cus_uid == cus_id);
                    txtFirstName.Text = cus.firstname;
                    txtLastName.Text = cus.lastname;
                    txtAddress.Text = cus.address;
                    txtPhone.Text = cus.phone;
                    dptBirth.SelectedDate = cus.birthdate;
                    rdMale.IsChecked = (cus.gender == 1);
                    rdFeMale.IsChecked = (cus.gender == 2);
                    lvOrder.ItemsSource = context.Orders.Where(x => x.cus_uid == cus_id).OrderBy(x => x.created).ToList();
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void btnDone_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                string firstname = txtFirstName.Text + "";
                string lastname = txtLastName.Text + "";
                string address = txtAddress.Text + "";
                string phone = txtPhone.Text + "";
                DateTime birth = dptBirth.SelectedDate.Value;
                short gender = (rdFeMale.IsChecked == true) ? (short)1 : (short)0;
                using(var context = new RESContext())
                {
                    Customer cus = context.Customers.FirstOrDefault(x => x.cus_uid == cus_id);
                    cus.firstname = firstname;
                    cus.lastname = lastname;
                    cus.address = address;
                    cus.phone = phone;
                    cus.birthdate = birth;
                    cus.gender = gender;

                    context.SaveChanges();
                }
            }
            catch(Exception ex)
            {

            }
            mdlMain.cus_id = cus_id;
            this.NavigationService.Navigate(new MainPage());
        }
    }
}
