﻿using RES.Common;
using RES.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for RegisterPage.xaml
    /// </summary>
    public partial class RegisterPage : Page
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainPage());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = txtName.Text + "";
                double open = 0;
                double.TryParse(txtAmount.Text + "", out open);
                using(var context = new RESContext())
                {
                    Register re = new Register()
                    {
                        re_uid = Guid.NewGuid(),
                        name = name,
                        openingamount = open,
                        cashsale = 0,
                        refunds = 0,
                        closeamount = 0,
                        start = DateTime.Now,
                        end = DateTime.Now,
                        emp_uid = mdlMain.emp_id
                    };
                    context.Registers.Add(re);
                    context.SaveChanges();
                    mdlMain.register_id = re.re_uid;
                }
            }
            catch(Exception ex)
            {

            }
            this.NavigationService.Navigate(new MainPage());
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    Register regis = context.Registers.OrderByDescending(x => x.start).FirstOrDefault();
                    if(regis != null){
                        txtOpen.Text = regis.start.ToString("dd/MM/yyyy HH:mm");
                        txtClose.Text = regis.end.ToString("dd/MM/yyyy HH:mm");
                        Employee emp = context.Employees.FirstOrDefault(x => x.emp_uid == regis.emp_uid);
                        txtEmployee.Text = emp.firstname + " " + emp.lastname;
                        txtOpenAmount.Text = regis.openingamount.ToString("###,###,###,##0") + " VND";
                        txtCashSale.Text = regis.cashsale.ToString("###,###,###,##0") + " VND";
                        txtRefund.Text = regis.refunds.ToString("###,###,###,##0") + " VND";
                    }
                    Employee currentEmp = context.Employees.FirstOrDefault(x => x.emp_uid == mdlMain.emp_id);
                    txtEmployeeName.Text = currentEmp.firstname + " " + currentEmp.lastname;
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}
