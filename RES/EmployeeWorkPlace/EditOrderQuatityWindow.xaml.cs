﻿using RES.Common;
using RES.Models;
using System;
using System.Linq;
using System.Windows;


namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for EditOrderQuatityWindow.xaml
    /// </summary>
    public partial class EditOrderQuatityWindow : Window
    {
        internal Guid or_id;
        public EditOrderQuatityWindow()
        {
            InitializeComponent();
        }

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    OrderDetail order = mdlMain.orderDetails.FirstOrDefault(x => x.ordetail_uid == or_id);
                    mdlMain.orderDetails.FirstOrDefault(x => x.ordetail_uid == or_id).quatity = Convert.ToInt32(txtQuatity.Text + "");
                }
            }
            catch (Exception ex)
            {

            }
            this.Close();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new RESContext())
                {
                    mdlMain.orderDetails.Remove(mdlMain.orderDetails.FirstOrDefault(x => x.ordetail_uid == or_id));
                }
            }
            catch (Exception ex)
            {

            }
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    OrderDetail order = mdlMain.orderDetails.FirstOrDefault(x => x.ordetail_uid == or_id);
                    txtQuatity.Text = order.quatity.ToString();
                    product.Text = context.Products.FirstOrDefault(x => x.product_uid == order.po_uid).name;
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}
