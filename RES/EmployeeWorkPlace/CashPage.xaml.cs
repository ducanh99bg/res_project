﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for CashPage.xaml
    /// </summary>
    public partial class CashPage : Page
    {
        internal bool quickSale;
        internal Customer cus;
        internal Order order;
        public CashPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using(var context = new RESContext())
                {
                    cus = context.Customers.FirstOrDefault(x => x.cus_uid == mdlMain.cus_id);
                    if(cus != null)
                    {
                        txtCustomer.Text = cus.firstname + " " + cus.lastname;
                    }
                    else
                    {
                        txtCustomer.Text = "Khách mua hàng nhanh";
                    }
                    txtAmount.Text = order.total.ToString();
                    txtType.Text = mdlMain.dicTypeSale[order.type].ToString();
                    lvOrdering.ItemsSource = mdlMain.orderDetails;
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainPage());
        }

        public void PdfFile()
        {
			try
			{
                string fontPath = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "\\Font\\ARIALUNI.TTF";
                Font f = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                f.Size = 10;
                f.SetStyle(Font.NORMAL);

                Font title = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                title.Size = 15;
                title.SetStyle(Font.NORMAL);

                Document pdfDoc = new Document(PageSize.A5, 20f, 20f, 40f, 40f);
                string path = $"E:\\bill\\bill.pdf";
                PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.Create));
                pdfDoc.Open();
                pdfDoc.AddTitle("Hóa đơn thanh toán");
                pdfDoc.Add(new Paragraph("HÓA ĐƠN THANH TOÁN", title)
                {
                    SpacingBefore = 40f,
                    SpacingAfter = 40f,
                    Alignment = Element.ALIGN_CENTER
                });
                var headerTable = new PdfPTable(new[] { .75f, 2f })
                {
                    WidthPercentage = 75,
                    DefaultCell = { MinimumHeight = 22f },
                    
                };

                var orderNumber = new Paragraph("Số hóa đơn:", f);
                var employee = new Paragraph("Thu ngân:", f);
                var customer = new Paragraph("Khách hàng:", f);
                var timeIn = new Paragraph("Giờ vào:", f);
                var timeOut = new Paragraph("Giờ ra:", f);

                headerTable.DefaultCell.Border = Rectangle.NO_BORDER;

                string employeeName = "";
                string customerName = "";
                using(var context = new RESContext())
                {
                    Employee emp = context.Employees.FirstOrDefault(x => x.emp_uid == order.emp_uid);
                    employeeName = emp.firstname + " " + emp.lastname;
                    if(cus != null)
                    {
                        customerName = cus.firstname + " " + cus.lastname;
                    }
                }

                headerTable.AddCell(orderNumber);
                headerTable.AddCell(order.or_uid.ToString());

                headerTable.AddCell(employee);
                headerTable.AddCell(new Paragraph(employeeName, f));

                headerTable.AddCell(customer);
                headerTable.AddCell(new Paragraph(customerName, f));

                headerTable.AddCell(timeIn);
                headerTable.AddCell(order.created.ToString("HH:mm"));

                headerTable.AddCell(timeOut);
                headerTable.AddCell(DateTime.Now.ToString("HH:mm"));
                pdfDoc.Add(headerTable);

                //Table 1
                var columnWidths = new[] { 2f, 1f, 1.5f, 1.5f };
                var table = new PdfPTable(columnWidths)
                {
                    WidthPercentage = 100,
                    DefaultCell = { MinimumHeight = 22f }

                };

                var product = new Paragraph("Món ăn", f);
                var quatity = new Paragraph("Số lượng", f);
                var price = new Paragraph("Đơn giá", f);
                var total = new Paragraph("Thành tiền", f);

                table.DefaultCell.Border = Rectangle.BOTTOM_BORDER;

                table.AddCell(product);
                table.AddCell(quatity);
                table.AddCell(price);
                table.AddCell(total);

                using (var context = new RESContext())
                {
                    List<OrderDetail> orders = context.OrderDetails.Where(x => x.or_uid == order.or_uid).ToList();
                    foreach(OrderDetail item in orders)
                    {
                        Product p = context.Products.FirstOrDefault(x => x.product_uid == item.po_uid);
                        var nameItem = new Paragraph(p.name, f);
                        var quatityItem = new Paragraph(item.quatity.ToString(), f);
                        var priceItem = new Paragraph(item.price.ToString("###,###,###") + " VND", f);
                        var totalItem = new Paragraph((item.price * item.quatity).ToString("###,###,###") + " VND", f);

                        table.AddCell(nameItem);
                        table.AddCell(quatityItem);
                        table.AddCell(priceItem);
                        table.AddCell(totalItem);
                    }
                }

                pdfDoc.Add(table);
                
                var columnWidth2 = new[] { 2f, 2f};
                var tableend = new PdfPTable(columnWidth2)
                {
                    WidthPercentage = 100,
                    DefaultCell = { MinimumHeight = 22f }

                };

                var discount = new Paragraph("Giảm giá:", f);
                var cash = new Paragraph("Thành tiền:", f);
                tableend.DefaultCell.Border = Rectangle.NO_BORDER;

                Order or = new Order();
                using (var context = new RESContext())
                {
                    or = context.Orders.SingleOrDefault(x => x.or_uid == order.or_uid);
                }

                tableend.AddCell(discount);
                tableend.AddCell(new Paragraph(or.discount + "%", f));
                tableend.AddCell(cash);
                tableend.AddCell(new Paragraph(or.total.ToString("###,###,###,###") + " VND", f));
                pdfDoc.Add(tableend);

                pdfDoc.Close();
                System.Diagnostics.Process.Start(path);
            }
			catch (Exception ex)
			{
                
			}
		}

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            using(var context = new RESContext())
            {
                double payValue = 0;
                double.TryParse(txtPay.Text + "", out payValue);
                if(payValue >= order.total)
                {
                    if (quickSale)
                    {
                        order.pay = payValue;
                        order.repay = order.pay - order.total;
                        order.status = 1;
                        context.Orders.Add(order);
                        foreach (OrderDetail item in mdlMain.orderDetails)
                        {
                            context.OrderDetails.Add(item);
                        }
                        context.SaveChanges();
                    }
                    else
                    {
                        var or = context.Orders.FirstOrDefault(x => x.or_uid == order.or_uid);
                        or.pay = payValue;
                        or.repay = order.pay - order.total;
                        or.status = 1;
                        context.SaveChanges();
                    }
                    PdfFile();
                    mdlMain.cus_id = new Guid();
                    mdlMain.order_id = new Guid();
                    mdlMain.orderDetails = new List<OrderDetail>();
                    this.NavigationService.Navigate(new MainPage());
                }
                else
                {
                    MessageBox.Show("Khoản tiền chi trả chưa đủ !");
                }
                
            }
            
        }

        private void txtPay_TextChanged(object sender, TextChangedEventArgs e)
        {
            double payValue = 0;
            double.TryParse(txtPay.Text + "", out payValue);
            if(payValue >= order.total)
            {
                txtRepay.Text = (payValue - order.total).ToString();
            }
            else
            {
                txtRepay.Text = "0";
            }
        }
    }
}
