﻿using RES.Common;
using RES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;


namespace RES.EmployeeWorkPlace
{
    /// <summary>
    /// Interaction logic for HistoryOrderPage.xaml
    /// </summary>
    public partial class HistoryOrderPage : Page
    {
        internal List<Order> orders;
        public HistoryOrderPage()
        {
            InitializeComponent();
        }

        public void SetItemSource()
        {
            try
            {
                DateTime date = dptDate.SelectedDate.Value;
                int status = cbxStatus.SelectedIndex;
                int type = cbxType.SelectedIndex;
                using(var context = new RESContext())
                {
                    orders = context.Orders.ToList();
                    if(date != null)
                    {
                        orders = orders.Where(x => x.created.Date == date.Date).ToList();
                    }
                    if(status != 0)
                    {
                        orders = orders.Where(x => x.status == status).ToList();
                    }
                    if(type != 0)
                    {
                        orders = orders.Where(x => x.type == type).ToList();
                    }
                }
                lvOrder.ItemsSource = orders;
            }
            catch(Exception ex)
            {

            }
        }
        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            dptDate.SelectedDate = DateTime.Now;
            cbxType.ItemsSource = mdlMain.dicTypeSale;
            cbxStatus.ItemsSource = mdlMain.dicStatusOrder;
            SetItemSource();
        }

        private void btnBack_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainPage());
        }

        private void cbxType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void cbxStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void dptDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            SetItemSource();
        }

        private void lvOrder_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                ListView items = sender as ListView;
                Order or = (Order)items.SelectedItem;
                if (or != null && or.status == 2)
                {
                    DetailOrderPage de = new DetailOrderPage();
                    de.order_uid = or.or_uid;
                    this.NavigationService.Navigate(de);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
