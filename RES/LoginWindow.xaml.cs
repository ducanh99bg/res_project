﻿using RES.AdminWorkPlace;
using RES.Common;
using RES.EmployeeWorkPlace;
using RES.Models;
using RES.WareHouse;
using System;
using System.Linq;
using System.Windows;


namespace RES
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string username = txtUserName.Text + "";
                string password = txtPassWord.Password + "";

                if (username == "admin" && password == "admin")
                {
                    AdminWindow admin = new AdminWindow();
                    admin.ShowDialog();
                }
                else
                {
                    using (var context = new RESContext())
                    {
                        Employee emp = context.Employees.FirstOrDefault(x => x.ac_uid == context.Accounts.FirstOrDefault(y => y.username == username && y.password == password).ac_uid);
                        if (emp == null)
                        {
                            MessageBox.Show("Sai tên đăng nhập hoặc mật  khẩu !");
                            return;
                        }
                        int position = context.Accounts.FirstOrDefault(x => x.ac_uid == emp.ac_uid).role;

                        switch (position)
                        {
                            case 1:
                                {
                                    mdlMain.emp_id = emp.emp_uid;
                                    EmployeeWindow employee = new EmployeeWindow();
                                    employee.ShowDialog();
                                    break;
                                }
                            case 2:
                                {
                                    mdlMain.emp_id = emp.emp_uid;
                                    EmployeeWindow employee = new EmployeeWindow();
                                    employee.ShowDialog();
                                    break;
                                }
                            case 3:
                                {
                                    mdlMain.emp_id = emp.emp_uid;
                                    EmployeeWindow employee = new EmployeeWindow();
                                    employee.ShowDialog();
                                    break;
                                }
                            case 4:
                                {
                                    mdlMain.emp_id = emp.emp_uid;
                                    WareHouseWindow warehouse = new WareHouseWindow();
                                    warehouse.ShowDialog();
                                    break;
                                }
                            case 5:
                                {
                                    mdlMain.emp_id = emp.emp_uid;
                                    AdminWindow admin = new AdminWindow();
                                    admin.ShowDialog();
                                    break;
                                }
                            default:
                                {
                                    break;
                                }
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
